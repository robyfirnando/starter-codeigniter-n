<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ourteam extends CI_Controller {


	public function __construct()
    {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Ourteammodel', 'model');
		is_login();
    }

	public function index()
	{
		echo 'ourteam';
	}

}

/* End of file Ourteam.php */
/* Location: ./application/modules/backend/controllers/Ourteam.php */
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends CI_Controller {
	
	public function __construct()
    {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Blogmodel', 'model');
		is_login();
		hak_akses();
    }
	
	public function index()
	{
		$status	= array('active', 'non-active');
		$data['success_message'] = $this->session->flashdata('success_message');
		
		$data['styles'] 	= '<link type="text/css" rel="stylesheet" href="'. base_url().'assets/backend/css/libs/DataTables/jquery.dataTables.css">
								<link type="text/css" rel="stylesheet" href="'. base_url().'assets/backend/css/libs/DataTables/extensions/dataTables.colVis.css">
								<link type="text/css" rel="stylesheet" href="'. base_url().'assets/backend/css/libs/DataTables/extensions/dataTables.tableTools.css">';
		$data['scripts'] 	= '<script src="'. base_url().'assets/backend/js/libs/DataTables/jquery.dataTables.min.js"></script>
								<script src="'. base_url().'assets/backend/js/libs/DataTables/extensions/ColVis/js/dataTables.colVis.min.js"></script>
								<script src="'. base_url().'assets/backend/js/libs/DataTables/extensions/TableTools/js/dataTables.tableTools.min.js"></script>
								<script src="'. base_url().'assets/backend/js/core/demo/DemoTableDynamic.js"></script>';
								
		$data['blogs']		= $this->model->get_all($status);
		$data['title']		= 'Blog';

		$this->template->load('template', 'blog/index', $data);
	}

	public function add()
	{
		$this->form_validation->set_rules('title', 'title', 'trim|required');
		$this->form_validation->set_rules('write_by', 'write_by', 'trim|required');

		if ( $this->form_validation->run() == true){

			if(!empty($_FILES['image']['size'])){
	    		$pic = do_upload_multiple($attachName='image', $path='./uploads/blog/');
	   		}else{
	    		$pic = '';
	   		}

			$this->model->insert($pic);
            $this->session->set_flashdata('success_message', "Add Blog success");
            redirect("backend/blog", 'refresh');

		}else{
			$data['message'] 	= (validation_errors() ? validation_errors() : $this->session->flashdata('message'));
			$data['title']		= 'Add Blog';
			$data['styles'] 	='';
			$data['scripts']	= '<script src="'. base_url().'assets/backend/js/core/demo/DemoFormComponents.js"></script>
							   		<script src="'. base_url().'assets/backend/js/libs/jquery-validation/dist/jquery.validate.min.js"></script>
							   		<script src="'. base_url().'assets/backend/js/libs/jquery-validation/dist/additional-methods.min.js"></script>';
			$data['categorys']	= $this->model->get_category();

			$this->template->load('template', 'blog/add', $data);
		}
	}

	public function edit($id)
	{
		$this->form_validation->set_rules('title', 'title', 'trim|required');
		$this->form_validation->set_rules('write_by', 'write_by', 'trim|required');

		

		if ( $this->form_validation->run() == true){

			if(!empty($_FILES['image']['size'])){

	    		$picdata = do_upload_multiple($attachName='image', $path='./uploads/blog/');

	   		}else{

	    		$picdata = '';

	   		}

			$this->model->update(to_Decrypt($id),$picdata);

            $this->session->set_flashdata('success_message', "Edit Blog success");

            redirect("backend/blog", 'refresh');

		}else{
			$data['message'] 	= (validation_errors() ? validation_errors() : $this->session->flashdata('message'));
			$data['styles'] 	='';
			$data['scripts']	= '<script src="'. base_url().'assets/backend/js/core/demo/DemoFormComponents.js"></script>
							   		<script src="'. base_url().'assets/backend/js/libs/jquery-validation/dist/jquery.validate.min.js"></script>
							   		<script src="'. base_url().'assets/backend/js/libs/jquery-validation/dist/additional-methods.min.js"></script>';
			$blog 				= $this->model->get_by_id(to_Decrypt($id));
			$data['blog'] 		= $blog[0];
			$data['detail']		= $this->model->get_by_id(to_Decrypt($id));
			$data['title']		= 'Edit Blog';
			$data['categorys']	= $this->model->get_category();

			$this->template->load('template', 'blog/edit', $data);
		}
	}

	public function delete($id)
	{
		$query = $this->model->delete(to_Decrypt($id));
		if($query){
			$this->session->set_flashdata('success_message', "Delete Blog success");
			redirect("backend/blog", 'refresh');
		}else{
			$this->session->set_flashdata('message', "Delete Blog error");
			redirect("backend/blog", 'refresh');
		}
	}

	public function deletedetail()
	{
		$detail_id 	 = $this->input->get('detail_id');
		$del 		 = $this->model->delete_detail($detail_id);

		if($del){
			$data['status'] = true;
		}else{
			$data['status'] = false;
		}
		echo json_encode($data);

	}



}
	
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {
	
	public function __construct()
    {
		parent::__construct();
		$this->load->library('form_validation');
		$this->load->model('Categorymodel', 'model');
		is_login();
		hak_akses();
    }
	
	public function index()
	{
		$status	= array('active', 'non-active');
        $data['success_message'] = $this->session->flashdata('success_message');
		
								
		$data['category']		= $this->model->get_all($status);
		$data['title']		= 'category';

		$this->template->load('template', 'category/index', $data);
	}

	public function add()
	{
		$this->form_validation->set_rules('name', 'Name', 'trim|required');
		// $this->form_validation->set_rules('status', 'Status', 'trim|required');
		

		if ( $this->form_validation->run() == true){

			$this->model->insert();
            $this->session->set_flashdata('success_message', "Add category success");
            redirect("backend/category", 'refresh');

		}else{
			$data['message'] 	= (validation_errors() ? validation_errors() : $this->session->flashdata('message'));
			$data['title']		= 'Add category';
			$data['styles'] 	='';
			$data['scripts']	= '<script src="'. base_url().'assets/backend/js/core/demo/DemoFormComponents.js"></script>
							   		<script src="'. base_url().'assets/backend/js/libs/jquery-validation/dist/jquery.validate.min.js"></script>
							   		<script src="'. base_url().'assets/backend/js/libs/jquery-validation/dist/additional-methods.min.js"></script>';
			
			$this->template->load('template', 'category/add', $data);
		}
	}

	public function edit($id)
	{
		$this->form_validation->set_rules('name', 'Name', 'trim|required');
		// $this->form_validation->set_rules('status', 'Status', 'trim|required');
		
		if ( $this->form_validation->run() == true){

			$this->model->update(to_Decrypt($id));
            $this->session->set_flashdata('success_message', "Update Category success");
            redirect("backend/category", 'refresh');

		}else{
			$data['message'] 	= (validation_errors() ? validation_errors() : $this->session->flashdata('message'));
			$data['styles'] 	='';
			$data['scripts']	= '<script src="'. base_url().'assets/backend/js/core/demo/DemoFormComponents.js"></script>
							   		<script src="'. base_url().'assets/backend/js/libs/jquery-validation/dist/jquery.validate.min.js"></script>
							   		<script src="'. base_url().'assets/backend/js/libs/jquery-validation/dist/additional-methods.min.js"></script>';
			$single 			= $this->model->get_by_id(to_Decrypt($id));
			$data['single'] 	= $single[0];
			$data['title']		= 'Edit Category';

			$this->template->load('template', 'category/edit', $data);
		}
	}

	public function delete($id)
	{
		$query = $this->model->delete(to_Decrypt($id));
		if($query){
			$this->session->set_flashdata('success_message', "Delete category success");
			redirect("backend/category", 'refresh');
		}else{
			$this->session->set_flashdata('message', "Delete category error");
			redirect("backend/category", 'refresh');
		}	}

	
}
	
 <script src="<?php echo base_url();?>assets/backend/js/libs/bootstrap-datepicker/bootstrap-datepicker.js"></script>
<div id="content">
	<section class="style-default-bright">
		<div class="section-header"></div>
		<div class="section-body contain-lg">
			<div class="row">
				<div class="col-lg-12 col-md-12">
					<?php if(isset($message)) { ?>
						<div class="alert alert-danger">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
							<?php echo $message ?>
						</div>
					<?php } ?>

					<form class="form form-validate floating-label" novalidate="novalidate" method="POST" id="form" enctype="multipart/form-data">

						<div class="card">
							<div class="card-head style-primary">
								<header>Form create new Gallery</header>
							</div>
							<div class="card-body">
								<div class="form-group">
									<input type="text" class="form-control" id="title" name="title" required data-rule-minlength="2" value="<?php echo isset($_POST['title']) ? $_POST['title'] : ''; ?>">
									<label for="title">Title</label>
								</div>
								<div class="form-group" id="place_image" style="display: none;">
			                      <img src="" id="image_category" style="width: 130px;height: 120px;">
			                  </div>

			                  <div class="form-group">
			                      <a class="btn btn-primary" id="btn_choose_image" onclick="$('#choose_image').click();">Choose Image</a>
			                      <input style="display: none;" type="file" id="choose_image" name="image"></input>
			                  </div>


								<div class="form-group">
									<div class="checkbox checkbox-styled">
										<label>
											<select name="status" class="form-control">
											<option value="">-Choose Status-</option>
											<option value="active">Active</option>
											<option value="nonactive">Non-Active</option>
											</select>
										</label>
									</div>
								</div>

							
							</div>
							<div class="card-actionbar">
								<div class="card-actionbar-row">
									<button type="reset" class="btn ink-reaction btn-flat btn-primary active reset">RESET</button>
									<button type="submit" class="btn ink-reaction btn-raised btn-primary submit">SUBMIT</button>
								</div>
							</div>
						</div>
						<em class="text-caption">Form create new Gallery</em>
					</form>
				</div>
			</div>
		</div>
		<div class="section-action style-primary">
			<div class="section-floating-action-row">
				<a class="btn ink-reaction btn-floating-action btn-lg btn-accent" href="<?php echo base_url().'backend/gallery' ?>" data-toggle="tooltip" data-placement="top" data-original-title="Back">
					<i class="md md-rotate-left"></i>
				</a>
			</div>
		</div>
	</section>
		</div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		
		$(document).on('blur, change', '#email', function(){
			$.ajax({
		        url 	: url+ 'backend/admins/check_email_admins',
		        type 	: 'POST',
		        data 	: 'email='+$(this).val(),
		        dataType: 'JSON',
		        beforeSend: function()
		        {
		        },
		        success: function(data)
		        {
		        	$('.error_dup').html(data.message);
		        	$('.error_dup').show();
		        	if(data.status){
		        		$('#form').find('.submit').attr('disabled', false);
		        	}else{
		        		$('#form').find('.submit').attr('disabled', true);
		        	}
		        },
		        error: function (jqXHR, textStatus, errorThrown)
		        {
		            alert('Error adding / update data');
		        }         
		    });
		    return false;
		});
		
		$(document).on('click', '.reset', function(){
		    $('.error_dup').hide();
		    $('.error_pass').hide();
		    $('.error_dup').html('');
			$('.error_pass').html('');
			$('#form').find('.submit').attr('disabled', false);
		});

		$(document).on('blur, change', '#confirm_password', function(){
			var pass 	= $('#password').val();
			var cpass 	= $(this).val();
			if(pass != cpass){
				$('.error_pass').show();
				$('.error_pass').html('Confirm password doesnt same');
				$('#form').find('.submit').attr('disabled', true);
			}else{
				$('.error_pass').hide();
				$('.error_pass').html('');
				$('#form').find('.submit').attr('disabled', false);
			}
		});

		$(document).on('change','#choose_image',function(){
        var el = $("#image_category");
        var files = !!this.files ? this.files : [];
        if (!files.length || !window.FileReader) return;
            if (/^image/.test( files[0].type)){ 
                var reader = new FileReader(); 
                reader.readAsDataURL(files[0]);
                reader.onloadend = function(){ 
                    el.attr("src",this.result);
                    $('#place_image').show();
                }
            }
    	});
		
	})	
</script>
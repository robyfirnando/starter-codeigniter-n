<div id="content">
	<section class="style-default-bright">
		<div class="section-header"></div>
		<div class="section-body contain-lg">
			<div class="row">
				<div class="col-lg-12 col-md-12">
					<?php if(isset($message)) { ?>
						<div class="alert alert-danger">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
							<?php echo $message ?>
						</div>
					<?php } ?>
					<form class="form form-validate floating-label" novalidate="novalidate" method="POST" id="form" enctype="multipart/form-data">
						<div class="card">
							<div class="card-head style-primary">
								<header>Form create new Blog</header>
							</div>
							<div class="card-body">
								<div class="form-group">
									<input type="text" class="form-control" id="title" name="title" required data-rule-minlength="2" value="<?php echo isset($_POST['title']) ? $_POST['title'] : ''; ?>">
									<label for="title">Title</label>
								</div>
								<div class="form-group">
									<input type="text" class="form-control" id="write_by" name="write_by" required value="<?php echo isset($_POST['write_by']) ? $_POST['write_by'] : ''; ?>">
									<label for="write_by">Write By</label>
								</div>
								<div class="form-group">
									<label for="description">Description</label>
									<textarea class="form-control ckeditor" id="description" name="description" data-rule-minlength="5" rows="4" ><?php echo isset($_POST['description']) ? $_POST['description'] : ''; ?></textarea>
								</div>
								<div class="form-group">
					                <input name="datetime" class="form-control datepicker" type="text">
					                <label for="datetime">Datetime</label>
					            </div>

					            <div class="form-group">
									<textarea class="form-control" id="tags" name="tags" data-rule-minlength="5" rows="4" ><?php echo isset($_POST['tags']) ? $_POST['tags'] : ''; ?></textarea>
									<label for="tags">Tags</label>
								</div>
								<div class="form-group">
									<input type="text" class="form-control" id="video_url" name="video_url" required value="<?php echo isset($_POST['video_url']) ? $_POST['video_url'] : ''; ?>">
									<label for="video_url">Video url</label>
								</div>

								<div class="form-group">
				              	<label for="category">Categoty</label>
				                    <select class="form-control" id="category" name="category">
				                    	<option value="">Choose Category</option>
				                    	<?php
											
											foreach($categorys as $us):
										?>
				                            <option value="<?php echo $us->id; ?>"><?php echo ucwords(str_replace("-", " ", $us->name)); ?></option>
				                            <?php
				                       endforeach;
				                        ?>
				                    </select>
				                </div>

				                <div class="form-group">
				              	<label for="status">Status</label>
				                    <select class="form-control" id="status" name="status">
				                    	<option value="">Choose Status</option>
				                    	<option value="active">Active</option>
				                    	<option value="non-active">Non-Active</option>
				                    </select>
				                </div>	

								<div class="form-group">
									<div class="checkbox checkbox-styled">
										<label>
											<input type="checkbox" name="featured">
											<span>Yes Featured</span>
										</label>
									</div>
								</div>

								<div class="row">
								<div class="placeimage">
									<span class="file-input btn btn-file photos">
										<a href="#" class="deleted-btn"><i class="fa fa-times"></i></a>
										<img class="border-white border-xl img-responsive auto-width" src="<?php echo base_url() ?>assets/backend/img/avatar6.jpg" alt="" style="width:600px; height:320px;">
										<input type="file" name="image[]" class="attach" multiple>
									</span>
									
								</div>
								
									<a type="button" class="btn ink-reaction btn-raised btn-primary moreimage">Add Image</a>
								</div>
							</div>
							<div class="card-actionbar">
								<div class="card-actionbar-row">
									<button type="reset" class="btn ink-reaction btn-flat btn-primary active reset">RESET</button>
									<button type="submit" class="btn ink-reaction btn-raised btn-primary submit">SUBMIT</button>
								</div>
							</div>
						</div>
						<em class="text-caption">Form create new Blog</em>
					</form>
				</div>
			</div>
		</div>
		<div class="section-action style-primary">
			<div class="section-floating-action-row">
				<a class="btn ink-reaction btn-floating-action btn-lg btn-accent" href="<?php echo base_url().'backend/blog' ?>" data-toggle="tooltip" data-placement="top" data-original-title="Back">
					<i class="md md-rotate-left"></i>
				</a>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
$('.moreimage').on('click', function(){
			$('.placeimage').append('<span class="file-input btn btn-file photos">'+
										'<a href="#" class="deleted-btn"><i class="fa fa-times"></i></a>'+
										'<img class="border-white border-xl img-responsive auto-width" src="<?php echo base_url() ?>assets/backend/img/avatar6.jpg" alt="" style="width:600px; height:320px;">'+
										'<input type="file" name="image[]" multiple class="attach">'+
										'<div class="form-group"  style="width:600px;text-align:left;">'+
										'<input type="text" class="form-control required" id="name_img" name="name_img[]" required value="<?php echo isset($_POST['name_img']) ? $_POST['name_img'] : ''; ?>">'+
										'<label for="name_img">Image Name</label>'+
									'</div>'+
									'</span>'
									)
		});

		$(document).on('change','.attach',function(){
		    var el = $(this).siblings('img');
		    var files = !!this.files ? this.files : [];
		    if (!files.length || !window.FileReader) return;
				if (/^image/.test( files[0].type)){ 
		            var reader = new FileReader(); 
		            reader.readAsDataURL(files[0]);
		            reader.onloadend = function(){ 
		                el.attr("src",this.result);
				}
		    }
		});

		$(document).on('click','.deleted-btn', function(){
			$(this).parent().remove();
			return false;
		});


		$(function () {
            $('.datepicker').datetimepicker({
			     format: 'YYYY-MM-DD HH:mm:ss A',
			});
        });
		
		$(document).on('click', '.reset', function(){
		    $('.error_dup').hide();
		    $('.error_pass').hide();
		    $('.error_dup').html('');
			$('.error_pass').html('');
			$('#form').find('.submit').attr('disabled', false);
		});

		$('#name_img').attr('required');

</script>


<div id="content">
	<section class="style-default-bright">
		<div class="section-header"></div>
		<div class="section-body contain-lg">
			<div class="row">
				<div class="col-lg-12 col-md-12">
					<?php if(isset($message)) { ?>
						<div class="alert alert-danger">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
							<?php echo $message ?>
						</div>
					<?php } ?>
					<form class="form form-validate floating-label" novalidate="novalidate" method="POST" id="form" enctype="multipart/form-data">
						<div class="card">
							<div class="card-head style-primary">
								<header>Update Blog <?php echo isset($blog['title']) ? $blog['title'] : ''; ?></header>
							</div>
							<div class="card-body">
								<div class="form-group">
									<input type="text" class="form-control" id="title" name="title" required data-rule-minlength="2" value="<?php echo isset($blog['title']) ? $blog['title'] : ''; ?>">
									<label for="title">Title</label>
								</div>
								<div class="form-group">
									<input type="text" class="form-control" id="write_by" name="write_by" required value="<?php echo isset($blog['write_by']) ? $blog['write_by'] : ''; ?>">
									<label for="write_by">Write By</label>
									<p class="help-block error_dup" style="color:red; display:none"></p>
								</div>
								<div class="form-group">
									<label for="description">Description</label>
									<textarea class="form-control ckeditor" id="description" name="description" data-rule-minlength="5" rows="4" ><?php echo isset($blog['description']) ? $blog['description'] : ''; ?></textarea>
								</div>
								<div class="form-group">
									<input name="datetime" class="form-control datepicker" type="text" value="<?php echo isset($blog['datetime']) ? $blog['datetime'] : ''; ?>">
									<label for="datetime">Datetime</label>
								</div>
								<div class="form-group">
									<textarea class="form-control" id="tags" name="tags" data-rule-minlength="5" rows="4" ><?php echo isset($blog['tags']) ? $blog['tags'] : ''; ?></textarea>
									<label for="tags">Tags</label>
								</div>
								<div class="form-group">
									<input type="text" class="form-control" id="video_url" name="video_url" required value="<?php echo isset($blog['video_url']) ? $blog['video_url'] : ''; ?>">
									<label for="video_url">Video url</label>
								</div>

								<div class="form-group">
				              	<label for="category">Categoty</label>
				                    <select class="form-control" id="category" name="category">
				                    	<option value="">Choose Category</option>
				                    	<?php
											$levels = $blog['category_id'];
											foreach($categorys as $us):
										?>
				                            <option value="<?php echo $us->id; ?>" <?php echo $us->id == $levels ? 'selected="selected"' : '' ?> ><?php echo ucwords(str_replace("-", " ", $us->name)); ?></option>
				                            <?php
				                       endforeach;
				                        ?>
				                    </select>
				                </div>

								<div class="form-group">
				              	<label for="status">Status</label>
								<?php
									$status = $blog['status'];
								?>
				                    <select class="form-control" id="status" name="status">
				                    	<option value="">Choose Status</option>
				                    	<option value="active" <?php if($status  == "active") echo "selected" ?> >Active</option>
				                    	<option value="non-active" <?php if($status  == "non-active") echo "selected" ?> >Non-Active</option>
				                    </select>
				                </div>	
								<div class="form-group">
									<div class="checkbox checkbox-styled">
										<label>
											<input type="checkbox" name="featured" <?php if($blog['featured'] == 'yes') echo 'checked'; ?>>
											<span>Yes Featured</span>
										</label>
									</div>
								</div>
								<div class="row">
							 
								<div class="placeimage">

									<?php 
										if(isset($detail)):
										foreach($detail as $aa): 

										if ($aa['image']) {
											# code...
										
									?>

									<div class="form-group">

										<span class="file-input btn btn-file photos">

											<input type="hidden"  name="image_id[]" value="<?php echo $aa['id_detail']; ?>">

											<img class="border-white border-xl img-responsive auto-width" src="<?php echo base_url($aa['image']); ?>" alt="" style="width:600px; height:320px;">

											<input type="file"  name="image_<?php echo $aa['id_detail']; ?>" value="<?php echo $aa['image']; ?>" class="attach" multiple>

										</span>

										<a href="" class="btn btn-icon-toggle btn-default delete_detail" detail_id="<?php echo $aa['id_detail']; ?>" data-toggle="tooltip" data-placement="top" title="Delete Image" >	

											<i class="md md-delete"></i>

										</a>

									</div>

									<?php 
										}
										endforeach; 
										endif;
									?>

								</div>
								
								
								<a type="button" class="btn ink-reaction btn-raised btn-primary moreimage">Add Image</a>		
								
							</div>
							</div>
							
							<div class="card-actionbar">
								<div class="card-actionbar-row">
									<button type="reset" class="btn ink-reaction btn-flat btn-primary active reset">RESET</button>
									<button type="submit" class="btn ink-reaction btn-raised btn-primary submit">SUBMIT</button>
								</div>
							</div>
						</div>
						<em class="text-caption">Form Edit Blog</em>
					</form>
				</div>
			</div>
		</div>
		<br><br><br>
		<div class="section-action style-primary">
			<div class="section-floating-action-row">
				<a class="btn ink-reaction btn-floating-action btn-lg btn-accent" href="<?php echo base_url().'backend/blog' ?>" data-toggle="tooltip" data-placement="top" data-original-title="Back">
					<i class="md md-rotate-left"></i>
				</a>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		
		$(function () {
            $('.datepicker').datetimepicker({
			     format: 'YYYY-MM-DD HH:mm:ss A',
			    
			});
        });
		
		
		$(document).on('click', '.reset', function(){
		    $('.error_dup').hide();
		    $('.error_pass').hide();
		    $('.error_dup').html('');
			$('.error_pass').html('');
			$('#form').find('.submit').attr('disabled', false);
		});

			$('.moreimage').on('click', function(){
			$('.placeimage').append('<span class="file-input btn btn-file photos">'+
										'<a href="#" class="deleted-btn"><i class="fa fa-times"></i></a>'+
										'<img class="border-white border-xl img-responsive auto-width" src="<?php echo base_url() ?>assets/backend/img/avatar6.jpg" alt="" style="width:600px; height:320px;">'+
										'<input type="file" name="image[]" multiple class="attach">'+
									'</span>'
									)
		});

		$(document).on('change','.attach',function(){
		    var el = $(this).siblings('img');
		    var files = !!this.files ? this.files : [];
		    if (!files.length || !window.FileReader) return;
				if (/^image/.test( files[0].type)){ 
		            var reader = new FileReader(); 
		            reader.readAsDataURL(files[0]);
		            reader.onloadend = function(){ 
		                el.attr("src",this.result);
				}	
		    }
		});

		$(document).on('click','.deleted-btn', function(){
			$(this).parent().remove();
			return false;
		});



		
	});

	  $(document).on('click', '.delete_detail', function(){

    	var url 		= "<?php echo base_url('backend/blog/deletedetail'); ?>"
    	var detail_id 	= $(this).attr('detail_id');
    	var element 	= $(this);
    	$.ajax({
	        url: url,
	        type: "GET",
	        data: 'detail_id='+detail_id,
	       	dataType : "JSON",
	        contentType: false,
	        cache: false,
	        processData:false,
	        beforeSend: function()
	        {

	        },
	        success: function(data)
	        {
	        	console.log(data.status);
	        	if(data.status == true){
	        		element.parent().remove();
	        	}else{
	        		alert('data gagal dihapus');
	        	}

	        },
	        error: function (jqXHR, textStatus, errorThrown)
	        {
	              alert('Error adding / update data');
	        }         
	      });


    	return false;


    })	
</script>

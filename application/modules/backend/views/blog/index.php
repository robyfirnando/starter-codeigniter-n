<div id="content">
	<section class="style-default-bright">
		<div class="section-header">
			<h2 class="text-primary">Listing data blog</h2>
		</div>
		<div class="section-body">
			<div class="row">
				<div class="col-md-8">
					<article class="margin-bottom-xxl">
						<p class="lead">
							Listing data blog
						</p>
					</article>
				</div>
			</div>
			<div class="row">
				<?php if(isset($message)): ?>
					<div class="alert alert-danger">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<?php echo $message ?>
					</div>
				<?php endif; ?>
				<?php if(isset($success_message)): ?>
					<div class="alert alert-success">
						<button type="button" class="close" data-dismiss="alert">&times;</button>
						<?php echo $success_message ?>
					</div>
				<?php endif; ?>
				<div class="col-lg-12">
					<div class="table-responsive">
						<table id="datatable1" class="table table-striped table-hover">
							<thead>
								<tr>
									<th class="sort-numeric">No</th>
									<th>Title</th>
									<th>Write By</th>
									<th>Description</th>
									<th>DateTime</th>
									<th>Tags</th>
									<th>Video Url</th>
									<th>Category</th>
									<th>Status</th>
									<th>Featured</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$no = 1;
									foreach($blogs as $blog):
								?>
								<tr>
									<td><?php echo $no; ?></td>
									<td>
										<?php 
										$nm = $blog->title;
	      								if (strlen($nm) > 10) $nm = substr($nm, 0, 15) . '...';
	      								echo $nm;
	      								?>
									</td>
									<td><?php echo $blog->write_by; ?></td>
									<td>
										<?php 
										$str = $blog->description;
	      								if (strlen($str) > 20) $str = substr($str, 0, 25) . '...';
	      								echo $str;
	      								?>
									</td>
									<td><?php echo $blog->datetime; ?></td>
									<td>
										<?php 
										$tgs = $blog->tags;
	      								if (strlen($tgs) > 10) $tgs = substr($tgs, 0, 15) . '...';
	      								echo $tgs;
	      								?>
									</td>
									<td>
										<?php 
										$vu = $blog->video_url;
	      								if (strlen($vu) > 10) $vu = substr($vu, 0, 15) . '...';
	      								echo $vu;
	      								?>
									</td>
									<td><?php echo $blog->namecategory; ?></td>
									<td><?php echo ucwords(str_replace("-", " ", $blog->status)); ?></td>
									<td><?php echo ucwords(str_replace("-", " ", $blog->featured)); ?></td>
									<td>
										<a href="<?php echo base_url().'backend/blog/edit/'.to_Encrypt($blog->id); ?>" class="btn ink-reaction btn-flat btn-primary active" data-toggle="tooltip" data-placement="top" data-original-title="Edit <?php echo $blog->title; ?>">
											<i class="md md-mode-edit"></i>
										</a>
										<a href="<?php echo base_url().'backend/blog/delete/'.to_Encrypt($blog->id); ?>" class="btn ink-reaction btn-primary delete" data-toggle="tooltip" data-placement="top" data-original-title="Delete <?php echo $blog->title; ?>">
											<i class="md md-delete"></i>
										</a>
									</td>
								</tr>
								<?php $no++; endforeach;?>
							</tbody>
						</table>
					</div><!--end .table-responsive -->
				</div>
			</div>
		</div>
		<br><br><br>
		<div class="section-action style-primary">
			<div class="section-floating-action-row">
				<a class="btn ink-reaction btn-floating-action btn-lg btn-accent" href="<?php echo base_url().'backend/blog/add' ?>" data-toggle="tooltip" data-placement="top" data-original-title="Add Blog">
					<i class="md md-add"></i>
				</a>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		$(document).on('click', '.delete', function(){

			var del_url =  $(this).attr('href');

			bootbox.confirm("<h4>Anda yakin ingin menghapus ?</h4>", function (result) {
		        if (result) {
		           location.href = del_url;
		        }
	    	});
	   	 	return false;
		})
	})
</script>
<div id="content">
	<section class="style-default-bright">
		<div class="section-header"></div>
		<div class="section-body contain-lg">
			<div class="row">
				<div class="col-lg-12 col-md-12">
					<?php if(isset($message)) { ?>
						<div class="alert alert-danger">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
							<?php echo $message ?>
						</div>
					<?php } ?>
					<form class="form form-validate floating-label" novalidate="novalidate" method="POST" id="form">
						<div class="card">
							<div class="card-head style-primary">
								<header>Form create new category</header>
							</div>
							<div class="card-body">
								<div class="form-group">
									<input type="text" class="form-control" id="name" name="name" required data-rule-minlength="2" value="<?php echo isset($_POST['name']) ? $_POST['name'] : ''; ?>">
									<label for="name">Name</label>
								</div>

							
									<div class="form-group">
									<div class="checkbox checkbox-styled">
										<label>
											<input type="checkbox" name="status">
											<span>Active Category</span>
										</label>
									</div>
								</div>

								
								

															<div class="card-actionbar">
								<div class="card-actionbar-row">
									<button type="reset" class="btn ink-reaction btn-flat btn-primary active reset">RESET</button>
									<button type="submit" class="btn ink-reaction btn-raised btn-primary submit">SUBMIT</button>
								</div>
							</div>
						</div>
						<em class="text-caption">Form create new category</em>
					</form>
				</div>
			</div>
		</div>
		<div class="section-action style-primary">
			<div class="section-floating-action-row">
				<a class="btn ink-reaction btn-floating-action btn-lg btn-accent" href="<?php echo base_url().'backend/category' ?>" data-toggle="tooltip" data-placement="top" data-original-title="Back">
					<i class="md md-rotate-left"></i>
				</a>
			</div>
		</div>
	</section>
</div>





<div id="content">
	<section>
		<div class="section-body">
			<div class="row">
				
				<!-- BEGIN ALERT - REVENUE -->
				<div class="col-md-3 col-sm-6">
					<div class="card">
						<div class="card-body no-padding">
							<div class="alert alert-callout alert-info no-margin">
								<strong class="pull-right text-success text-lg">0,38% <i class="md md-trending-up"></i></strong>
								<strong class="text-xl">$ 32,829</strong><br>
								<span class="opacity-50">Revenue</span>
								<div class="stick-bottom-left-right">
									<div class="height-2 sparkline-revenue" data-line-color="#bdc1c1"><canvas width="428" height="80" style="display: inline-block; width: 428px; height: 80px; vertical-align: top;"></canvas></div>
								</div>
							</div>
						</div><!--end .card-body -->
					</div><!--end .card -->
				</div><!--end .col -->

				
				<!-- END ALERT - REVENUE -->
				
				<!-- BEGIN ALERT - VISITS -->
				<div class="col-md-3 col-sm-6">
					<div class="card">
						<div class="card-body no-padding">
							<div class="alert alert-callout alert-warning no-margin">
								<strong class="pull-right text-warning text-lg">0,01% <i class="md md-swap-vert"></i></strong>
								<strong class="text-xl">432,901</strong><br>
								<span class="opacity-50">Visits</span>
								<div class="stick-bottom-right">
									<div class="height-1 sparkline-visits" data-bar-color="#e5e6e6"><canvas width="413" height="40" style="display: inline-block; width: 413px; height: 40px; vertical-align: top;"></canvas></div>
								</div>
							</div>
						</div><!--end .card-body -->
					</div><!--end .card -->
				</div><!--end .col -->
				<!-- END ALERT - VISITS -->
				
				<!-- BEGIN ALERT - BOUNCE RATES -->
				<div class="col-md-3 col-sm-6">
					<div class="card">
						<div class="card-body no-padding">
							<div class="alert alert-callout alert-danger no-margin">
								<strong class="pull-right text-danger text-lg">0,18% <i class="md md-trending-down"></i></strong>
								<strong class="text-xl">42.90%</strong><br>
								<span class="opacity-50">Bounce rate</span>
								<div class="stick-bottom-left-right">
									<div class="progress progress-hairline no-margin">
										<div class="progress-bar progress-bar-danger" style="width:43%"></div>
									</div>
								</div>
							</div>
						</div><!--end .card-body -->
					</div><!--end .card -->
				</div><!--end .col -->
				<!-- END ALERT - BOUNCE RATES -->
				
				<!-- BEGIN ALERT - TIME ON SITE -->
				<div class="col-md-3 col-sm-6">
					<div class="card">
						<div class="card-body no-padding">
							<div class="alert alert-callout alert-success no-margin">
								<h1 class="pull-right text-success"><i class="md md-timer"></i></h1>
								<strong class="text-xl">54 sec.</strong><br>
								<span class="opacity-50">Avg. time on site</span>
							</div>
						</div><!--end .card-body -->
					</div><!--end .card -->
				</div><!--end .col -->
				<!-- END ALERT - TIME ON SITE -->
				
			</div><!--end .row -->
			<div class="row">
				
				<!-- BEGIN SITE ACTIVITY -->
				<div class="col-md-9">
					<div class="card ">
						<div class="row">
							<div class="col-md-8">
								<div class="card-head">
									<header>Site activity</header>
								</div><!--end .card-head -->
								<div class="card-body height-8">
									<div id="flot-visitors-legend" class="flot-legend-horizontal stick-top-right no-y-padding"><table style="font-size:smaller;color:rgb(49, 53, 52)"><tbody><tr><td class="legendColorBox"><div style="border:1px solid #ccc;padding:1px"><div style="width:4px;height:0;border:5px solid rgb(125,216,210);overflow:hidden"></div></div></td><td class="legendLabel">Pageviews</td></tr><tr><td class="legendColorBox"><div style="border:1px solid #ccc;padding:1px"><div style="width:4px;height:0;border:5px solid rgb(10,168,158);overflow:hidden"></div></div></td><td class="legendLabel">Visitors</td></tr></tbody></table></div>
									<div id="flot-visitors" class="flot height-7" data-title="Activity entry" data-color="#7dd8d2,#0aa89e" style="width: 100%; padding: 0px; position: relative;"><canvas class="flot-base" width="836" height="280" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 836px; height: 280px;"></canvas><div class="flot-text" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; font-size: smaller; color: rgb(84, 84, 84);"><div class="flot-x-axis flot-x1-axis xAxis x1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; display: block;"><div style="position: absolute; max-width: 92px; top: 268px; font-style: normal; font-variant: normal; font-weight: 400; font-stretch: normal; font-size: 10px; line-height: 12px; font-family: Roboto, sans-serif, Helvetica, Arial, sans-serif; color: rgb(49, 53, 52); left: 98px; text-align: center;">11 May</div><div style="position: absolute; max-width: 92px; top: 268px; font-style: normal; font-variant: normal; font-weight: 400; font-stretch: normal; font-size: 10px; line-height: 12px; font-family: Roboto, sans-serif, Helvetica, Arial, sans-serif; color: rgb(49, 53, 52); left: 211px; text-align: center;">12 May</div><div style="position: absolute; max-width: 92px; top: 268px; font-style: normal; font-variant: normal; font-weight: 400; font-stretch: normal; font-size: 10px; line-height: 12px; font-family: Roboto, sans-serif, Helvetica, Arial, sans-serif; color: rgb(49, 53, 52); left: 324px; text-align: center;">13 May</div><div style="position: absolute; max-width: 92px; top: 268px; font-style: normal; font-variant: normal; font-weight: 400; font-stretch: normal; font-size: 10px; line-height: 12px; font-family: Roboto, sans-serif, Helvetica, Arial, sans-serif; color: rgb(49, 53, 52); left: 438px; text-align: center;">14 May</div><div style="position: absolute; max-width: 92px; top: 268px; font-style: normal; font-variant: normal; font-weight: 400; font-stretch: normal; font-size: 10px; line-height: 12px; font-family: Roboto, sans-serif, Helvetica, Arial, sans-serif; color: rgb(49, 53, 52); left: 551px; text-align: center;">15 May</div><div style="position: absolute; max-width: 92px; top: 268px; font-style: normal; font-variant: normal; font-weight: 400; font-stretch: normal; font-size: 10px; line-height: 12px; font-family: Roboto, sans-serif, Helvetica, Arial, sans-serif; color: rgb(49, 53, 52); left: 664px; text-align: center;">16 May</div><div style="position: absolute; max-width: 92px; top: 268px; font-style: normal; font-variant: normal; font-weight: 400; font-stretch: normal; font-size: 10px; line-height: 12px; font-family: Roboto, sans-serif, Helvetica, Arial, sans-serif; color: rgb(49, 53, 52); left: 777px; text-align: center;">17 May</div></div><div class="flot-y-axis flot-y1-axis yAxis y1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; display: block;"><div style="position: absolute; top: 257px; font-style: normal; font-variant: normal; font-weight: 400; font-stretch: normal; font-size: 10px; line-height: 12px; font-family: Roboto, sans-serif, Helvetica, Arial, sans-serif; color: rgb(49, 53, 52); left: 16px; text-align: right;">0</div><div style="position: absolute; top: 206px; font-style: normal; font-variant: normal; font-weight: 400; font-stretch: normal; font-size: 10px; line-height: 12px; font-family: Roboto, sans-serif, Helvetica, Arial, sans-serif; color: rgb(49, 53, 52); left: 5px; text-align: right;">200</div><div style="position: absolute; top: 155px; font-style: normal; font-variant: normal; font-weight: 400; font-stretch: normal; font-size: 10px; line-height: 12px; font-family: Roboto, sans-serif, Helvetica, Arial, sans-serif; color: rgb(49, 53, 52); left: 5px; text-align: right;">400</div><div style="position: absolute; top: 104px; font-style: normal; font-variant: normal; font-weight: 400; font-stretch: normal; font-size: 10px; line-height: 12px; font-family: Roboto, sans-serif, Helvetica, Arial, sans-serif; color: rgb(49, 53, 52); left: 5px; text-align: right;">600</div><div style="position: absolute; top: 53px; font-style: normal; font-variant: normal; font-weight: 400; font-stretch: normal; font-size: 10px; line-height: 12px; font-family: Roboto, sans-serif, Helvetica, Arial, sans-serif; color: rgb(49, 53, 52); left: 5px; text-align: right;">800</div><div style="position: absolute; top: 2px; font-style: normal; font-variant: normal; font-weight: 400; font-stretch: normal; font-size: 10px; line-height: 12px; font-family: Roboto, sans-serif, Helvetica, Arial, sans-serif; color: rgb(49, 53, 52); left: 0px; text-align: right;">1000</div></div></div><canvas class="flot-overlay" width="836" height="280" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 836px; height: 280px;"></canvas></div>
								</div><!--end .card-body -->
							</div><!--end .col -->
							<div class="col-md-4">
								<div class="card-head">
									<header>Today's stats</header>
								</div>
								<div class="card-body height-8">
									<strong>214</strong> members
									<span class="pull-right text-success text-sm">0,18% <i class="md md-trending-up"></i></span>
									<div class="progress progress-hairline">
										<div class="progress-bar progress-bar-primary-dark" style="width:43%"></div>
									</div>
									756 pageviews
									<span class="pull-right text-success text-sm">0,68% <i class="md md-trending-up"></i></span>
									<div class="progress progress-hairline">
										<div class="progress-bar progress-bar-primary-dark" style="width:11%"></div>
									</div>
									291 bounce rates
									<span class="pull-right text-danger text-sm">21,08% <i class="md md-trending-down"></i></span>
									<div class="progress progress-hairline">
										<div class="progress-bar progress-bar-danger" style="width:93%"></div>
									</div>
									32,301 visits
									<span class="pull-right text-success text-sm">0,18% <i class="md md-trending-up"></i></span>
									<div class="progress progress-hairline">
										<div class="progress-bar progress-bar-primary-dark" style="width:63%"></div>
									</div>
									132 pages
									<span class="pull-right text-success text-sm">0,18% <i class="md md-trending-up"></i></span>
									<div class="progress progress-hairline">
										<div class="progress-bar progress-bar-primary-dark" style="width:47%"></div>
									</div>
								</div><!--end .card-body -->
							</div><!--end .col -->
						</div><!--end .row -->
					</div><!--end .card -->		
				</div><!--end .col -->
				<!-- END SITE ACTIVITY -->
				
				<!-- BEGIN SERVER STATUS -->
				<div class="col-md-3">
					<div class="card">
						<div class="card-head">
							<header class="text-primary">Server status</header>
						</div><!--end .card-head -->
						<div class="card-body height-4">
							<div class="pull-right text-center">
								<em class="text-primary">Temperature</em>
								<br>
								<div id="serverStatusKnob" class="knob knob-shadow knob-primary knob-body-track size-2">
									<div style="display:inline;width:80px;height:80px;"><canvas width="80" height="80"></canvas><input type="text" class="dial" data-min="0" data-max="100" data-thickness=".09" value="50" data-readonly="true" readonly="readonly" style="width: 44px; height: 26px; position: absolute; vertical-align: middle; margin-top: 26px; margin-left: -62px; border: 0px; font-style: normal; font-variant: normal; font-weight: bold; font-stretch: normal; font-size: 16px; line-height: normal; font-family: Arial; text-align: center; color: rgb(10, 168, 158); padding: 0px; -webkit-appearance: none; background: none;"></div>
								</div>
							</div>
						</div><!--end .card-body -->
						<div class="card-body height-4 no-padding">
							<div class="stick-bottom-left-right">
								<div id="rickshawGraph" class="height-4 rickshaw_graph" data-color1="#0aa89e" data-color2="rgba(79, 89, 89, 0.2)"><svg width="430" height="160"><g><path d="M0,1.5841584158415856L5.810810810810811,23.163228351203372L11.621621621621623,22.835446387424014L17.432432432432435,12.715496611941433L23.243243243243246,20.15012601220343L29.054054054054056,21.845225451478683L34.86486486486487,40.401446441892546L40.67567567567568,38.06099165866337L46.48648648648649,27.33283969898929L52.2972972972973,30.174914138370326L58.10810810810811,32.181901742791595L63.91891891891892,13.74976531291344L69.72972972972974,17.533163447095518L75.54054054054055,26.646021376143125L81.35135135135135,23.9395632874581L87.16216216216216,37.47789741236983L92.97297297297298,39.88459733121883L98.78378378378379,34.646597909295664L104.5945945945946,33.576831593598584L110.4054054054054,44.24024657667229L116.21621621621622,38.68120903541666L122.02702702702702,25.221512843442866L127.83783783783784,27.439166313693278L133.64864864864865,17.01773270319819L139.45945945945948,20.323632998242232L145.27027027027026,23.239430940277003L151.0810810810811,29.595086800239812L156.8918918918919,9.075077772540226L162.7027027027027,5.96852985674218L168.51351351351352,8.581348225827412L174.32432432432432,19.0596620170501L180.13513513513513,16.78261050120514L185.94594594594597,33.558978075948545L191.75675675675674,28.778560936771896L197.56756756756758,27.53840075674367L203.3783783783784,18.17263702801941L209.1891891891892,39.58347559271343L215,31.940262198506048L220.8108108108108,27.09595660213023L226.62162162162164,28.843094501124142L232.43243243243245,30.500159069173776L238.24324324324326,41.87518223765743L244.05405405405403,29.695504631667774L249.8648648648649,49.28000246381896L255.67567567567568,61.16291267446715L261.4864864864865,47.544990473718826L267.2972972972973,36.924888974161135L273.1081081081081,40.75968200056917L278.91891891891896,54.28632956350228L284.72972972972974,59.94591461218086L290.5405405405405,54.79046721918989L296.35135135135135,60.221753461904314L302.1621621621622,60.68019853604659L307.97297297297297,66.5863722923547L313.7837837837838,53.50893349423262L319.5945945945946,51.37408668972057L325.4054054054054,51.24203465863022L331.2162162162162,49.792410395751844L337.02702702702703,59.02328655980442L342.8378378378378,52.708812023043265L348.64864864864865,57.11321697008333L354.4594594594595,38.68465960825132L360.27027027027026,47.20454523513055L366.08108108108104,46.27548726190952L371.89189189189193,52.294219485554976L377.7027027027027,58.112112598271665L383.5135135135135,46.2869738684081L389.3243243243243,68.69566966648395L395.13513513513516,54.16197514347287L400.94594594594594,44.45250550204668L406.7567567567568,48.51646974405881L412.56756756756755,60.13080204539091L418.3783783783784,54.850004355389814L424.1891891891892,52.40468242677677L430,35.169808334011464L430,93.08891280662709L424.1891891891892,108.55052328586612L418.3783783783784,105.67709603083189L412.56756756756755,108.65140010114447L406.7567567567568,105.72710586302915L400.94594594594594,95.1531677589238L395.13513513513516,96.79800116128192L389.3243243243243,111.951668767814L383.5135135135135,98.60002246145147L377.7027027027027,102.30730399545165L371.89189189189193,94.49314343642287L366.08108108108104,100.6713486895968L360.27027027027026,106.11450577917648L354.4594594594595,95.47420059179443L348.64864864864865,108.25806529075214L342.8378378378378,104.0103377022541L337.02702702702703,108.06129898307765L331.2162162162162,106.3373886448301L325.4054054054054,95.51706309375308L319.5945945945946,98.22343117961718L313.7837837837838,104.66617152835028L307.97297297297297,111.2873297736268L302.1621621621622,108.56542902007607L296.35135135135135,101.16640995276762L290.5405405405405,103.77670113447009L284.72972972972974,104.25075187188179L278.91891891891896,97.00764640856664L273.1081081081081,98.0494663797196L267.2972972972973,95.13643541272381L261.4864864864865,105.56223351899617L255.67567567567568,107.33699492222027L249.8648648648649,106.43764651908882L244.05405405405403,90.99339003075482L238.24324324324326,103.31764137359468L232.43243243243245,89.59581990891202L226.62162162162164,90.46286704968405L220.8108108108108,86.35150925057509L215,91.86619533565687L209.1891891891892,97.58261675478434L203.3783783783784,86.11119703795387L197.56756756756758,97.32341901741287L191.75675675675674,95.1496382122491L185.94594594594597,93.99445927568259L180.13513513513513,84.47188691023345L174.32432432432432,90.42881745392978L168.51351351351352,81.27686529013978L162.7027027027027,81.11580957036176L156.8918918918919,78.69823968278702L151.0810810810811,90.89408245781249L145.27027027027026,89.90410804355629L139.45945945945948,94.04777085557816L133.64864864864865,81.399530569387L127.83783783783784,96.12237744394938L122.02702702702702,85.08366898367414L116.21621621621622,98.21831731936177L110.4054054054054,101.13730676367624L104.5945945945946,98.7700131707463L98.78378378378379,90.0106578007903L92.97297297297298,96.61211992353635L87.16216216216216,94.38714889300013L81.35135135135135,95.65431190019905L75.54054054054055,91.9118248586436L69.72972972972974,90.35148687576564L63.91891891891892,88.09836843541903L58.10810810810811,97.26619607566779L52.2972972972973,99.94236730984004L46.48648648648649,87.6479143370212L40.67567567567568,100.39486362715587L34.86486486486487,101.83154025393469L29.054054054054056,85.40500809440178L23.243243243243246,91.0472835934419L17.432432432432435,90.86550632274603L11.621621621621623,96.23570892404425L5.810810810810811,89.30627808633007L0,81.00508777313587Z" class="area" fill="rgba(79, 89, 89, 0.2)"></path></g><g><path d="M0,81.00508777313587L5.810810810810811,89.30627808633007L11.621621621621623,96.23570892404425L17.432432432432435,90.86550632274603L23.243243243243246,91.0472835934419L29.054054054054056,85.40500809440178L34.86486486486487,101.83154025393469L40.67567567567568,100.39486362715587L46.48648648648649,87.6479143370212L52.2972972972973,99.94236730984004L58.10810810810811,97.26619607566779L63.91891891891892,88.09836843541903L69.72972972972974,90.35148687576564L75.54054054054055,91.9118248586436L81.35135135135135,95.65431190019905L87.16216216216216,94.38714889300013L92.97297297297298,96.61211992353635L98.78378378378379,90.0106578007903L104.5945945945946,98.7700131707463L110.4054054054054,101.13730676367624L116.21621621621622,98.21831731936177L122.02702702702702,85.08366898367414L127.83783783783784,96.12237744394938L133.64864864864865,81.399530569387L139.45945945945948,94.04777085557816L145.27027027027026,89.90410804355629L151.0810810810811,90.89408245781249L156.8918918918919,78.69823968278702L162.7027027027027,81.11580957036176L168.51351351351352,81.27686529013978L174.32432432432432,90.42881745392978L180.13513513513513,84.47188691023345L185.94594594594597,93.99445927568259L191.75675675675674,95.1496382122491L197.56756756756758,97.32341901741287L203.3783783783784,86.11119703795387L209.1891891891892,97.58261675478434L215,91.86619533565687L220.8108108108108,86.35150925057509L226.62162162162164,90.46286704968405L232.43243243243245,89.59581990891202L238.24324324324326,103.31764137359468L244.05405405405403,90.99339003075482L249.8648648648649,106.43764651908882L255.67567567567568,107.33699492222027L261.4864864864865,105.56223351899617L267.2972972972973,95.13643541272381L273.1081081081081,98.0494663797196L278.91891891891896,97.00764640856664L284.72972972972974,104.25075187188179L290.5405405405405,103.77670113447009L296.35135135135135,101.16640995276762L302.1621621621622,108.56542902007607L307.97297297297297,111.2873297736268L313.7837837837838,104.66617152835028L319.5945945945946,98.22343117961718L325.4054054054054,95.51706309375308L331.2162162162162,106.3373886448301L337.02702702702703,108.06129898307765L342.8378378378378,104.0103377022541L348.64864864864865,108.25806529075214L354.4594594594595,95.47420059179443L360.27027027027026,106.11450577917648L366.08108108108104,100.6713486895968L371.89189189189193,94.49314343642287L377.7027027027027,102.30730399545165L383.5135135135135,98.60002246145147L389.3243243243243,111.951668767814L395.13513513513516,96.79800116128192L400.94594594594594,95.1531677589238L406.7567567567568,105.72710586302915L412.56756756756755,108.65140010114447L418.3783783783784,105.67709603083189L424.1891891891892,108.55052328586612L430,93.08891280662709L430,160L424.1891891891892,160L418.3783783783784,160L412.56756756756755,160L406.7567567567568,160L400.94594594594594,160L395.13513513513516,160L389.3243243243243,160L383.5135135135135,160L377.7027027027027,160L371.89189189189193,160L366.08108108108104,160L360.27027027027026,160L354.4594594594595,160L348.64864864864865,160L342.8378378378378,160L337.02702702702703,160L331.2162162162162,160L325.4054054054054,160L319.5945945945946,160L313.7837837837838,160L307.97297297297297,160L302.1621621621622,160L296.35135135135135,160L290.5405405405405,160L284.72972972972974,160L278.91891891891896,160L273.1081081081081,160L267.2972972972973,160L261.4864864864865,160L255.67567567567568,160L249.8648648648649,160L244.05405405405403,160L238.24324324324326,160L232.43243243243245,160L226.62162162162164,160L220.8108108108108,160L215,160L209.1891891891892,160L203.3783783783784,160L197.56756756756758,160L191.75675675675674,160L185.94594594594597,160L180.13513513513513,160L174.32432432432432,160L168.51351351351352,160L162.7027027027027,160L156.8918918918919,160L151.0810810810811,160L145.27027027027026,160L139.45945945945948,160L133.64864864864865,160L127.83783783783784,160L122.02702702702702,160L116.21621621621622,160L110.4054054054054,160L104.5945945945946,160L98.78378378378379,160L92.97297297297298,160L87.16216216216216,160L81.35135135135135,160L75.54054054054055,160L69.72972972972974,160L63.91891891891892,160L58.10810810810811,160L52.2972972972973,160L46.48648648648649,160L40.67567567567568,160L34.86486486486487,160L29.054054054054056,160L23.243243243243246,160L17.432432432432435,160L11.621621621621623,160L5.810810810810811,160L0,160Z" class="area" fill="#0aa89e"></path></g></svg><div class="detail"></div></div>
							</div>
						</div><!--end .card-body -->
					</div><!--end .card -->		
				</div><!--end .col -->
				<!-- END SERVER STATUS -->
				
			</div><!--end .row -->
			<div class="row">
				
				<!-- BEGIN TODOS -->
				<div class="col-md-3">
					<div class="card ">
						<div class="card-head">
							<header>Todo's</header>
							<div class="tools">
								<a class="btn btn-icon-toggle btn-close"><i class="md md-close"></i></a>
							</div>
						</div><!--end .card-head -->
						<div class="nano has-scrollbar" style="height: 360px;"><div class="nano-content" tabindex="0" style="right: -17px;"><div class="card-body no-padding height-9 scroll" style="height: auto;">
							<ul class="list" data-sortable="true">
								<li class="tile">
									<div class="checkbox checkbox-styled tile-text">
										<label>
											<input type="checkbox" checked="">
											<span>Call clients for follow-up</span>
										</label>
									</div>
									<a class="btn btn-flat ink-reaction btn-default">
										<i class="md md-delete"></i>
									</a>
								</li>
								<li class="tile">
									<div class="checkbox checkbox-styled tile-text">
										<label>
											<input type="checkbox">
											<span>
												Schedule meeting
												<small>opportunity for new customers</small>
											</span>
										</label>
									</div>
									<a class="btn btn-flat ink-reaction btn-default">
										<i class="md md-delete"></i>
									</a>
								</li>
								<li class="tile">
									<div class="checkbox checkbox-styled tile-text">
										<label>
											<input type="checkbox">
											<span>
												Upload files to server
												<small>The files must be shared with all members of the board</small>
											</span>
										</label>
									</div>
									<a class="btn btn-flat ink-reaction btn-default">
										<i class="md md-delete"></i>
									</a>
								</li>
								<li class="tile">
									<div class="checkbox checkbox-styled tile-text">
										<label>
											<input type="checkbox">
											<span>Forward important tasks</span>
										</label>
									</div>
									<a class="btn btn-flat ink-reaction btn-default">
										<i class="md md-delete"></i>
									</a>
								</li>
								<li class="tile">
									<div class="checkbox checkbox-styled tile-text">
										<label>
											<input type="checkbox">
											<span>Forward important tasks</span>
										</label>
									</div>
									<a class="btn btn-flat ink-reaction btn-default">
										<i class="md md-delete"></i>
									</a>
								</li>
								<li class="tile">
									<div class="checkbox checkbox-styled tile-text">
										<label>
											<input type="checkbox">
											<span>Forward important tasks</span>
										</label>
									</div>
									<a class="btn btn-flat ink-reaction btn-default">
										<i class="md md-delete"></i>
									</a>
								</li>
							</ul>
						</div></div><div class="nano-pane"><div class="nano-slider" style="height: 333px; transform: translate(0px, 0px);"></div></div></div><!--end .card-body -->
					</div><!--end .card -->
				</div><!--end .col -->
				<!-- END TODOS -->
				
				<!-- BEGIN REGISTRATION HISTORY -->
				<div class="col-md-6">
					<div class="card">
						<div class="card-head">
							<header>Registration history</header>
							<div class="tools">
								<a class="btn btn-icon-toggle btn-refresh"><i class="md md-refresh"></i></a>
								<a class="btn btn-icon-toggle btn-collapse"><i class="fa fa-angle-down"></i></a>
								<a class="btn btn-icon-toggle btn-close"><i class="md md-close"></i></a>
							</div>
						</div><!--end .card-head -->
						<div class="card-body no-padding height-9">
							<div class="row">
								<div class="col-sm-6 hidden-xs">
									<div class="force-padding text-sm text-default-light">
										<p>
											<i class="md md-mode-comment text-primary-light"></i>
											The registrations are measured from the time that the redesign was fully implemented and after the first e-mailing.
										</p>
									</div>
								</div><!--end .col -->
								<div class="col-sm-6">
									<div class="force-padding pull-right text-default-light">
										<h2 class="no-margin text-primary-dark"><span class="text-xxl">66.05%</span></h2>
										<i class="fa fa-caret-up text-success fa-fw"></i> more registrations
									</div>
								</div><!--end .col -->
							</div><!--end .row -->
							<div class="stick-bottom-left-right force-padding">
								<div id="flot-registrations" class="flot height-5" data-title="Registration history" data-color="#0aa89e" style="width: 100%; padding: 0px; position: relative;"><canvas class="flot-base" width="836" height="200" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 836px; height: 200px;"></canvas><div class="flot-text" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; font-size: smaller; color: rgb(84, 84, 84);"><div class="flot-x-axis flot-x1-axis xAxis x1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; display: block;"><div style="position: absolute; max-width: 64px; top: 188px; font-style: normal; font-variant: normal; font-weight: 400; font-stretch: normal; font-size: 10px; line-height: 12px; font-family: Roboto, sans-serif, Helvetica, Arial, sans-serif; color: rgb(49, 53, 52); left: 46px; text-align: center;">Jul 15</div><div style="position: absolute; max-width: 64px; top: 188px; font-style: normal; font-variant: normal; font-weight: 400; font-stretch: normal; font-size: 10px; line-height: 12px; font-family: Roboto, sans-serif, Helvetica, Arial, sans-serif; color: rgb(49, 53, 52); left: 117px; text-align: center;">Aug 15</div><div style="position: absolute; max-width: 64px; top: 188px; font-style: normal; font-variant: normal; font-weight: 400; font-stretch: normal; font-size: 10px; line-height: 12px; font-family: Roboto, sans-serif, Helvetica, Arial, sans-serif; color: rgb(49, 53, 52); left: 191px; text-align: center;">Sep 15</div><div style="position: absolute; max-width: 64px; top: 188px; font-style: normal; font-variant: normal; font-weight: 400; font-stretch: normal; font-size: 10px; line-height: 12px; font-family: Roboto, sans-serif, Helvetica, Arial, sans-serif; color: rgb(49, 53, 52); left: 263px; text-align: center;">Oct 15</div><div style="position: absolute; max-width: 64px; top: 188px; font-style: normal; font-variant: normal; font-weight: 400; font-stretch: normal; font-size: 10px; line-height: 12px; font-family: Roboto, sans-serif, Helvetica, Arial, sans-serif; color: rgb(49, 53, 52); left: 335px; text-align: center;">Nov 15</div><div style="position: absolute; max-width: 64px; top: 188px; font-style: normal; font-variant: normal; font-weight: 400; font-stretch: normal; font-size: 10px; line-height: 12px; font-family: Roboto, sans-serif, Helvetica, Arial, sans-serif; color: rgb(49, 53, 52); left: 406px; text-align: center;">Dec 15</div><div style="position: absolute; max-width: 64px; top: 188px; font-style: normal; font-variant: normal; font-weight: 400; font-stretch: normal; font-size: 10px; line-height: 12px; font-family: Roboto, sans-serif, Helvetica, Arial, sans-serif; color: rgb(49, 53, 52); left: 480px; text-align: center;">Jan 16</div><div style="position: absolute; max-width: 64px; top: 188px; font-style: normal; font-variant: normal; font-weight: 400; font-stretch: normal; font-size: 10px; line-height: 12px; font-family: Roboto, sans-serif, Helvetica, Arial, sans-serif; color: rgb(49, 53, 52); left: 553px; text-align: center;">Feb 16</div><div style="position: absolute; max-width: 64px; top: 188px; font-style: normal; font-variant: normal; font-weight: 400; font-stretch: normal; font-size: 10px; line-height: 12px; font-family: Roboto, sans-serif, Helvetica, Arial, sans-serif; color: rgb(49, 53, 52); left: 621px; text-align: center;">Mar 16</div><div style="position: absolute; max-width: 64px; top: 188px; font-style: normal; font-variant: normal; font-weight: 400; font-stretch: normal; font-size: 10px; line-height: 12px; font-family: Roboto, sans-serif, Helvetica, Arial, sans-serif; color: rgb(49, 53, 52); left: 695px; text-align: center;">Apr 16</div><div style="position: absolute; max-width: 64px; top: 188px; font-style: normal; font-variant: normal; font-weight: 400; font-stretch: normal; font-size: 10px; line-height: 12px; font-family: Roboto, sans-serif, Helvetica, Arial, sans-serif; color: rgb(49, 53, 52); left: 764px; text-align: center;">May 16</div></div><div class="flot-y-axis flot-y1-axis yAxis y1Axis" style="position: absolute; top: 0px; left: 0px; bottom: 0px; right: 0px; display: block;"><div style="position: absolute; top: 177px; font-style: normal; font-variant: normal; font-weight: 400; font-stretch: normal; font-size: 10px; line-height: 12px; font-family: Roboto, sans-serif, Helvetica, Arial, sans-serif; color: rgb(49, 53, 52); left: 16px; text-align: right;">0</div><div style="position: absolute; top: 133px; font-style: normal; font-variant: normal; font-weight: 400; font-stretch: normal; font-size: 10px; line-height: 12px; font-family: Roboto, sans-serif, Helvetica, Arial, sans-serif; color: rgb(49, 53, 52); left: 0px; text-align: right;">2000</div><div style="position: absolute; top: 90px; font-style: normal; font-variant: normal; font-weight: 400; font-stretch: normal; font-size: 10px; line-height: 12px; font-family: Roboto, sans-serif, Helvetica, Arial, sans-serif; color: rgb(49, 53, 52); left: 0px; text-align: right;">4000</div><div style="position: absolute; top: 46px; font-style: normal; font-variant: normal; font-weight: 400; font-stretch: normal; font-size: 10px; line-height: 12px; font-family: Roboto, sans-serif, Helvetica, Arial, sans-serif; color: rgb(49, 53, 52); left: 0px; text-align: right;">6000</div><div style="position: absolute; top: 2px; font-style: normal; font-variant: normal; font-weight: 400; font-stretch: normal; font-size: 10px; line-height: 12px; font-family: Roboto, sans-serif, Helvetica, Arial, sans-serif; color: rgb(49, 53, 52); left: 0px; text-align: right;">8000</div></div></div><canvas class="flot-overlay" width="836" height="200" style="direction: ltr; position: absolute; left: 0px; top: 0px; width: 836px; height: 200px;"></canvas></div>
							</div>
						</div><!--end .card-body -->
					</div><!--end .card -->
				</div><!--end .col -->
				<!-- END REGISTRATION HISTORY -->
				
				<!-- BEGIN TODOS -->
				<div class="col-md-3">
					<div class="card ">
						<div class="card-head">
							<header>New registrations</header>
							<div class="tools">
								<a class="btn btn-icon-toggle btn-close"><i class="md md-close"></i></a>
							</div>
						</div><!--end .card-head -->
						<div class="nano has-scrollbar" style="height: 360px;"><div class="nano-content" tabindex="0" style="right: -17px;"><div class="card-body no-padding height-9 scroll" style="height: auto;">
							<ul class="list" data-sortable="true">
							<?php
								foreach($users as $user):
							?>
								<li class="tile" id="user">
									<div class="checkbox checkbox-styled tile-text">
										<label>
											 <input type="checkbox" class="status" name="status" <?php if($user->status == 'active') echo 'checked'; ?> detail_id="<?php echo $user->id; ?>" status="<?php echo $user->status; ?>"> 
											<span>
												<?php echo $user->name; ?>
												<small><?php echo ucwords(str_replace("-", " ", $user->nama)); ?></small>
											</span>
										</label>
									</div>
									<a class="btn btn-flat ink-reaction btn-default">
										<a href="" class="btn btn-flat ink-reaction btn-default delete_detail" detail_id="<?php echo $user->id; ?>" data-toggle="tooltip" data-placement="top" title="Delete User" >	
												<i class="md md-delete "></i>
											</a> 
									</a>
								</li>
							<?php endforeach;?>	
							</ul>
						</div></div><div class="nano-pane"><div class="nano-slider" style="height: 333px; transform: translate(0px, 0px);"></div></div></div><!--end .card-body -->
					</div><!--end .card -->
				</div><!--end .col -->
				<!-- END TODOS -->

				<!-- BEGIN REGISTRATION BLOG -->
				<div class="col-md-6">
					<div class="card">
						<div class="card-head">
							<header>Listing New Blog</header>
							<div class="tools">
								<a class="btn btn-icon-toggle btn-refresh"><i class="md md-refresh"></i></a>
								<a class="btn btn-icon-toggle btn-collapse"><i class="fa fa-angle-down"></i></a>
								<a class="btn btn-icon-toggle btn-close"><i class="md md-close"></i></a>
							</div>
						</div><!--end .card-head -->
						<div class="nano has-scrollbar" style="height: 360px;"><div class="nano-content" tabindex="0" style="right: -17px;"><div class="card-body no-padding height-9 scroll" style="height: auto;">
							<ul class="list divider-full-bleed">
							<?php
								foreach($blogs as $blog):
							?>
								<li class="tile">
									<div class="tile-content">
										<div class="tile-text"><?php echo $blog->title; ?></br>
										<small><i>write by <?php echo $blog->write_by; ?></i></br>
												<?php 
													$str = $blog->description;
				      								if (strlen($str) > 100) $str = substr($str, 0, 120) . '...';
				      								echo $str;
			      								?>
											</small>
										</div>
									</div>
									<a class="btn btn-flat ink-reaction">
										<i class="fa fa-angle-down show"></i>
									</a>
								</li>
							<?php endforeach;?>	
							</ul>
							
						</div></div><div class="nano-pane"><div class="nano-slider" style="height: 333px; transform: translate(0px, 0px);"></div></div></div><!--end .card-body -->
					</div><!--end .card -->
				</div><!--end .col -->
				<!-- END REGISTRATION BLOG -->
				
			</div><!--end .row -->
		</div><!--end .section-body -->
	</section>
</div><!--end #content-->
<script type="text/javascript">



	$(document).on('click', '.delete_detail', function(){

    	var url 		= "<?php echo base_url('dashboard/deleteuser'); ?>"
    	var detail_id 	= $(this).attr('detail_id');
    	var element 	= $(this);
    	$.ajax({
	        url: url,
	        type: "GET",
	        data: 'detail_id='+detail_id,
	       	dataType : "JSON",
	        contentType: false,
	        cache: false,
	        processData:false,
	        beforeSend: function()
	        {

	        },
	        success: function(data)
	        {
	        	console.log(data.status);
	        	if(data.status == true){
	        		element.parent().remove();
	        	}else{
	        		alert('data gagal dihapus');
	        	}

	        },
	        error: function (jqXHR, textStatus, errorThrown)
	        {
	              alert('Error adding / update data');
	        }         
	      });


    	return false;


    })

    $(document).on('change', '.status', function(){

    	var url 		= "<?php echo base_url('dashboard/updateuser'); ?>"
    	var detail_id 	= $(this).attr('detail_id');
    	var status 	= $(this).attr('status');
    	var element 	= $(this);
    	var data_string = 'detail_id='+ detail_id +'&status='+ status;
    	$.ajax({
	        url: url,
	        type: "GET",
	        data: data_string,
	       	dataType : "JSON",
	        contentType: false,
	        cache: false,
	        processData:false,
	        beforeSend: function()
	        {

	        },
	        success: function(data)
	        {
	        	console.log(data.status);
	        	if(data.status == true){
	        		alert('sukses');
	        	}else{
	        		alert('data gagal dihapus');
	        	}

	        },
	        error: function (jqXHR, textStatus, errorThrown)
	        {
	              alert('Error adding / update data');
	        }         
	      });


    	return false;


    })

</script>
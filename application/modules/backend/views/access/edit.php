<div id="content">
	<section class="style-default-bright">
		<div class="section-header"></div>
		<div class="section-body contain-lg">
			<div class="row">
				<div class="col-lg-12 col-md-12">
					<?php if(isset($message)) { ?>
						<div class="alert alert-danger">
							<button type="button" class="close" data-dismiss="alert">&times;</button>
							<?php echo $message ?>
						</div>
					<?php } ?>
					<form class="form form-validate floating-label" novalidate="novalidate" method="POST" id="form" enctype="multipart/form-data">
						<div class="card">
							<div class="card-head style-primary">
								<header>Update Level Access <?php echo isset($akses['nama']) ? $akses['nama'] : ''; ?></header>
							</div>
							<div class="card-body">
								<div class="form-group">
				                    <label>Name</label>
				                    <select id="nama" name="nama" class="form-control">
				                        <option value=""></option>
				                            <?php
												$id = $akses['id_level'];
												foreach($view as $us):
											?>
					                            <option value="<?php echo $us->id; ?>" <?php echo $us->id == $id ? 'selected="selected"' : '' ?> ><?php echo $us->nama; ?></option>
					                            <?php
					                       endforeach;
					                        ?>
				                    </select>
				            	</div>
				            	<div class="form-group">
									<textarea class="form-control" rows="6" name="hak_akses"> <?php echo $akses['hak_akses'] ?></textarea>
										<label for="desc">Address</label>
								</div>
							</div>
							<div class="card-actionbar">
								<div class="card-actionbar-row">
									<button type="reset" class="btn ink-reaction btn-flat btn-primary active reset">RESET</button>
									<button type="submit" class="btn ink-reaction btn-raised btn-primary submit">SUBMIT</button>
								</div>
							</div>
						</div>
						<em class="text-caption">Form Edit Level Access</em>
					</form>
				</div>
			</div>
		</div>
		<br><br><br>
		<div class="section-action style-primary">
			<div class="section-floating-action-row">
				<a class="btn ink-reaction btn-floating-action btn-lg btn-accent" href="<?php echo base_url().'backend/access' ?>" data-toggle="tooltip" data-placement="top" data-original-title="Back">
					<i class="md md-rotate-left"></i>
				</a>
			</div>
		</div>
	</section>
</div>
<script type="text/javascript">
	$(document).ready(function(){
		
		$(document).on('click', '.reset', function(){
		    $('.error_dup').hide();
		    $('.error_pass').hide();
		    $('.error_dup').html('');
			$('.error_pass').html('');
			$('#form').find('.submit').attr('disabled', false);
		});
		
	});	
</script>
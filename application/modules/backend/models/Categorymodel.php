<?php

class Categorymodel extends CI_Model
{   
    public function __construct()
    {
        parent::__construct();
    }

    public function insert()
    {
        $status             = ($this->input->post("status") == 'on') ? 'active' : 'non-active' ;
        $name               = $this->input->post("name");
        
        $this->db->set("name", $name)
                 ->set("status", $status)
                 ->set("slug", "")
                 ->set("created_at", date("Y-m-d H:i:s"))
                 ->set("created_by", $this->session->userdata('back_userid'))
                 ->insert("category");

                $id   = $this->db->insert_id();
        $slug    = strtolower($name.' '.$id);
        $slug    = trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $slug));
        $this->db->where('id', $id);
        $this->db->set('slug', $slug)->update("category");

        return true;
    }

    public function update($id)
    {
        $name               = $this->input->post("name");
        $status             = ($this->input->post("status") == 'on') ? 'active' : 'non-active' ;
        $slug               = strtolower($name.' '.$id);
        $slug               = trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $slug));

        
            $this->db->where("id", $id)
                     ->set("name", $name)
                     ->set("status", $status)
                     ->set("slug", $slug)
                     ->set("modified_at", date("Y-m-d H:i:s"))
                     ->set("modified_by", $this->session->userdata('back_userid'))
                     ->update("category");
        
        return true;
    }
    
    public function delete($id)
    {
        $this->db->where("id", $id);
        $this->db->set("status", "deleted");
        $this->db->update("category");
        return true; 
    }

    public function get_by_id($id)
    {
        $this->db->select("*");
        $this->db->from("category");
        $this->db->where("id", $id);
        $query  = $this->db->get();
        $data   = array();    
        if ($query->num_rows() > 0)
        {
            foreach (($query->result_array()) as $row) $data[] = $row;
            return $data;
        }
    }

    public function get_all($status)
    {
        $admin =$this->get_level_id('category');
        $this->db->select("*");
        if(count($status) > 0)
        {
            $i = 1;
            foreach ($status as $st) 
            {
                if($i == 1){
                    $this->db->where("status", $st);
                    // $this->db->where("id_level", $admin );
                }else{
                    $this->db->or_where("status", $st);
                    // $this->db->where("id_level", $admin);
                }
                $i++;
            }
        }
        $this->db->from("category");
        $query  = $this->db->get();
        $data   = $query->result();
        return $data;
    }


    public function check_email($email)
    {
        $this->db->select("count(0) as cnt");
        $this->db->from("login");
        $this->db->where("email", $email);
        $qry = $this->db->get()->result();
        return $qry ? $qry[0]->cnt:0;
    }

    public function check_email_edit($email, $uri_email)
    {
        $this->db->select("count(0) as cnt");
        $this->db->from("login");
        $this->db->where("email", $email);
        $this->db->where("email !=", $uri_email);
        $qry = $this->db->get()->result();
        return $qry ? $qry[0]->cnt:0;
    }

        public function cek_akses($id_level)
    {   
        $this->db->select('hak_akses');
        $this->db->from('level_access');
        $this->db->where('id_level', $id_level);
        $this->db->limit(1);
        $query = $this->db->get();
        $akses = $query->result();

        return $akses[0]->hak_akses;
    }

    public function get_level_id($name)
    {
        $this->db->select("id as cnt");
        $this->db->from("levels");
        $this->db->where("nama", $name);
        $qry = $this->db->get()->result();
       
        return $qry ? $qry[0]->cnt:0;
    
    }

}
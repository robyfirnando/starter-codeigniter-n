<?php

class Gallerymodel extends CI_Model
{   
    public function __construct()
    {
        parent::__construct();
    }

    public function insert()
    {
        $title               = $this->input->post("title");
        $image               = $this->input->post("image");
        $status              = $this->input->post("status");

if($_FILES['image']['size'] > 0)
        {
            $pic   = do_upload_single('image', './uploads/gallery/');
            $this->db->set("image", 'uploads/gallery/'.$pic['data']['file_name']);
        }

        $this->db->set("title", $title)
                 ->set("status", $status)
                 ->set("created_at", date("Y-m-d H:i:s"))
                 ->set("created_by", $this->session->userdata('back_userid'))
                 ->insert("gallery");
    }




    public function update($id)
    { 
      
        $title               = $this->input->post("title");
        $type                = $this->input->post("type");
        $status              = $this->input->post("status");

        if($_FILES['image']['size'] > 0)
                {
                    $pic   = do_upload_single('image', './uploads/gallery/');
                    $this->db->set("image", 'uploads/gallery/'.$pic['data']['file_name']);
        }

       $this->db->where("id",$id)
                 ->set("title", $title)
                 ->set("status", $status)
                 ->set("modified_at", date("Y-m-d H:i:s"))
                 ->set("modified_by", $this->session->userdata('back_userid'))
                 ->update("gallery");



      
        return true;
    }
    
    public function delete($id)
    {
        $this->db->where("id", $id);
        $this->db->set("status", "deleted");
        $this->db->update("gallery");
        return true; 
    }

    public function get_by_id($id)
    {
        $this->db->select("*");
        $this->db->from("gallery");
        $this->db->where("id", $id);
        $query  = $this->db->get();
        $data   = array();    
        if ($query->num_rows() > 0)
        {
            foreach (($query->result_array()) as $row) $data[] = $row;
            return $data;
        }
    }

    public function get_all($status)
    {
        $this->db->select("*");
        if(count($status) > 0)
        {
            $i = 1;
            foreach ($status as $st) 
            {
                if($i == 1){
                    $this->db->where("status", $st);
                }else{
                    $this->db->or_where("status", $st);
                }
                $i++;
            }
        }
        $this->db->from("gallery");
        $query  = $this->db->get();
        $data   = $query->result();
        return $data;
    }

<<<<<<< HEAD
        public function cek_akses($id_level)
    {   

=======
       public function cek_akses($id_level)
    {   
>>>>>>> a5f6702a3c8b0eec69f74ab8f4a053dfedac85ac
        $this->db->select('hak_akses');
        $this->db->from('level_access');
        $this->db->where('id_level', $id_level);
        $this->db->limit(1);
        $query = $this->db->get();
        $akses = $query->result();

<<<<<<< HEAD

=======
>>>>>>> a5f6702a3c8b0eec69f74ab8f4a053dfedac85ac
        return $akses[0]->hak_akses;
    }

}
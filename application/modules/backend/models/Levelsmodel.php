<?php

class Levelsmodel extends CI_Model
{   
    public function __construct()
    {
        parent::__construct();
    }
    public function insert()
    {
        $name   = $this->input->post("nama");
      
        $this->db->set("nama", $name)
                 ->set("created_at", date("Y-m-d H:i:s"))
                 ->set("created_by", $this->session->userdata('back_userid'))
                 ->insert("levels");
        return true;
    }

    public function update($id)
    {
        $name   = $this->input->post("nama");

        $this->db->where("id", $id)
                ->set("nama", $name)
                ->set("modified_at", date("Y-m-d H:i:s"))
                ->set("modified_by", $this->session->userdata('back_userid'))
                ->update("levels");

        return true;
    }
    
    public function delete($id)
    {
        $this->db->where('id', $id)
                 ->delete('levels');
        return true; 
    }

    public function get_by_id($id)
    {
        $this->db->select("*");
        $this->db->from("levels");
        $this->db->where("id", $id);
        $query  = $this->db->get();
        $data   = array();    
        if ($query->num_rows() > 0)
        {
            foreach (($query->result_array()) as $row) $data[] = $row;
            return $data;
        }
    }

    public function get_all()
    {
        $this->db->select("*");
        $this->db->from("levels");
        $query  = $this->db->get();
        $data   = $query->result();
        return $data;
    }

    public function cek_akses($id_level)
    {   
        $this->db->select('hak_akses');
        $this->db->from('level_access');
        $this->db->where('id_level', $id_level);
        $this->db->limit(1);
        $query = $this->db->get();
        $akses = $query->result();

        return $akses[0]->hak_akses;
    }



}
<?php

class Dashboardmodel extends CI_Model
{   
    public function __construct()
    {
        parent::__construct();
    }

        public function updateuser($id,$status)
        {
            if ($status == "active") {
                $status = "non-active";
            }else{
                $status = "active";
            }
            $this->db->where("id", $id)
                    ->set("status", $status)
                    ->set("modified_at", date("Y-m-d H:i:s"))
                    ->set("modified_by", $this->session->userdata('back_userid'))
                    ->update("login");

            return true;
        }
    
    public function deleteuser($id)
    {
       $this->db->where('id', $id)
                 ->delete('login');
        return true; 
    }


    public function get_user()
    {
        $this->db->select("login.*,levels.nama");
        $this->db->from("login");
        $this->db->join("levels",'login.id_level = levels.id');
        $this->db->order_by('id','desc');
        $query  = $this->db->get();
        $data   = $query->result();
        return $data;
    }

    public function get_blog($status)
    {
        $this->db->select("*");
        if(count($status) > 0)
        {
            $i = 1;
            foreach ($status as $st) 
            {
                if($i == 1){
                    $this->db->where("status", $st);
                    $this->db->where("type","blog");
                    $this->db->order_by('id','desc');
                }else{
                    $this->db->or_where("status", $st);
                    $this->db->where("type","blog");
                    $this->db->order_by('id','desc');
                }
                $i++;
            }   
        }
        $this->db->from("article");
        $query  = $this->db->get();
        $data   = $query->result();
        return $data;
    }

    public function cek_akses($id_level)
    {   

        $this->db->select('hak_akses');
        $this->db->from('level_access');
        $this->db->where('id_level', $id_level);
        $this->db->limit(1);
        $query = $this->db->get();
        $akses = $query->result();

        return $akses[0]->hak_akses;
    }



   



}
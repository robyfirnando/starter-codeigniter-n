<?php

class Accessmodel extends CI_Model
{   
    public function __construct()
    {
        parent::__construct();
    }

    public function cek_akses($id_level)
    {   

        $this->db->select('hak_akses');
        $this->db->from('level_access');
        $this->db->where('id_level', $id_level);
        $this->db->limit(1);
        $query = $this->db->get();
        $akses = $query->result();


        return $akses[0]->hak_akses;
    }


    public function insert()
    {
        $akses      = $this->input->post("hak_akses");
        $id_level   = $this->input->post("nama");
      
        $this->db->set("id_level", $id_level)
                 ->set("hak_akses", $akses)
                 ->set("created_at", date("Y-m-d H:i:s"))
                 ->set("created_by", $this->session->userdata('back_userid'))
                 ->insert("level_access");
        return true;
    }

    public function update($id)
    {
        $akses      = $this->input->post("hak_akses");
        $id_level   = $this->input->post("nama");

        $this->db->where("id", $id)
                ->set("id_level", $id_level)
                ->set("hak_akses", $akses)
                ->set("modified_at", date("Y-m-d H:i:s"))
                ->set("modified_by", $this->session->userdata('back_userid'))
                ->update("level_access");

        return true;
    }
    
    public function delete($id)
    {
        $this->db->where('id', $id)
                 ->delete('level_access');
        return true; 
    }

    public function get_by_id($id)
    {
        $this->db->select("level_access.*,levels.nama");
        $this->db->from("level_access");
        $this->db->join("levels",'level_access.id_level = levels.id');
        $this->db->where("level_access.id", $id);
        $query  = $this->db->get();
        $data   = array();    
        if ($query->num_rows() > 0)
        {
            foreach (($query->result_array()) as $row) $data[] = $row;
            return $data;
        }
    }

    public function get_all()
    {
        $this->db->select("level_access.*,levels.nama");
        $this->db->from("level_access");
        $this->db->join("levels",'level_access.id_level = levels.id');
        $query  = $this->db->get();
        $data   = $query->result();
        return $data;
    }

    public function levels()
    {
        $this->db->select("*");
        $this->db->from("levels");
        $query  = $this->db->get();
        $data   = $query->result();
        return $data;
    }

   



}
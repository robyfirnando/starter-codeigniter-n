<?php

class Blogmodel extends CI_Model
{   
    public function __construct()
    {
        parent::__construct();
    }

    public function insert($pic)
    {
        
        $featured           = ($this->input->post("featured") == 'on') ? 'yes' : 'no' ;
        $title              = $this->input->post("title");
        $write_by           = $this->input->post("write_by");
        $description        = $this->input->post("description");
        $datetime           = $this->input->post("datetime");
        $tags               = $this->input->post("tags");
        $video_url          = $this->input->post("video_url");
        $category           = $this->input->post("category");
        $status             = $this->input->post("status");


        $this->db->set("title", $title)
                 ->set("write_by", $write_by)
                 ->set("description", $description)
                 ->set("datetime", $datetime)
                 ->set("status", $status)
                 ->set("tags", $tags)
                 ->set("video_url", $video_url)
                 ->set("category_id", $category)
                 ->set("type", "blog")
                 ->set("featured", $featured)
                 ->set("slug", "")
                 ->set("created_at", date("Y-m-d H:i:s"))
                 ->set("created_by", $this->session->userdata('back_userid'))
                 ->insert("article");

        $id   = $this->db->insert_id();
        $slug    = strtolower($title.' '.$id);
        $slug    = trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $slug));
        $this->db->where('id', $id);
        $this->db->set('slug', $slug)->update("article");


        foreach ($pic['status'] as $key => $value) {
            if(!empty($id) && $value == 'success')  
            {
                $this->db->set("name", $title );
                $this->db->set("image", 'uploads/blog/'.$pic['data'][$key]['file_name'] );
                $this->db->set("parent_id", $id);
                $this->db->set("type", 'blog');
                $this->db->set("created_at", date("Y-m-d H:i:s"));
                $this->db->set("created_by", $this->session->userdata('back_userid'));
                $this->db->insert("images");
               
            }
        }


        return true;
    }

    public function update($id,$picdata)
    {
        error_reporting(0);
        $img_id             = $this->input->post('image_id');
        $featured           = ($this->input->post("featured") == 'on') ? 'yes' : 'no' ;
        $title              = $this->input->post("title");
        $write_by           = $this->input->post("write_by");
        $description        = $this->input->post("description");
        $datetime           = $this->input->post("datetime");
        $status             = $this->input->post("status");
        $slug               = strtolower($title.' '.$id);
        $slug               = trim(preg_replace('/[^A-Za-z0-9-]+/', '-', $slug));
        $tags               = $this->input->post("tags");
        $video_url          = $this->input->post("video_url");
        $category           = $this->input->post("category");

        

        $this->db->where("id", $id)
                    ->set("title", $title)
                    ->set("write_by", $write_by)
                    ->set("description", $description)
                    ->set("datetime", $datetime)
                    ->set("status", $status)
                    ->set("slug", $slug)
                    ->set("tags", $tags)
                    ->set("video_url", $video_url)
                    ->set("category_id", $category)
                    ->set("featured", $featured)
                    ->set("modified_at", date("Y-m-d H:i:s"))
                    ->set("modified_by", $this->session->userdata('back_userid'))
                    ->update("article");

          if(!empty($img_id))

        {

            foreach ($img_id as $key => $value) {

                if($value != 0)

                {

                    $size =  $_FILES['image'.$value]['size'];

                    if($size > 0)

                    {

                        $pic = do_upload_single('image_'.$value, './uploads/blog/');





                        $this->db->where('id', $value);

                        $this->db->set("name", $title);

                        $this->db->set("image", 'uploads/blog/'.$pic['data']['file_name'] );

                        $this->db->set("modified_at", date("Y-m-d H:i:s"));

                        $this->db->set("modified_by", $this->session->userdata('back_userid'));

                        $this->db->update("images");



                    }

                }

                        

            }

        }



        if($picdata != "")

        {

            foreach ($picdata['status'] as $key => $value) {

                if(!empty($id) && $value == 'success')  

                {   

                $this->db->set("name", $title );

                $this->db->set("image", 'uploads/blog/'.$picdata['data'][$key]['file_name'] );

                $this->db->set("parent_id", $id);

                $this->db->set("type", 'blog');

                $this->db->set("created_at", date("Y-m-d H:i:s"));

                $this->db->set("created_by", $this->session->userdata('back_userid'));

                $this->db->insert("images");



                }



            }

        }


    }

    
    public function delete($id)
    {
        $this->db->where("id", $id);
        $this->db->set("status", "deleted");
        $this->db->update("article");
        return true; 
    }

    public function get_by_id($id)
    {
        $this->db->select("article.*,images.name as name_img,images.image,images.id as id_detail");
        $this->db->from("article");
        $this->db->join('images', 'article.id = images.parent_id','left');
        $this->db->where("article.id", $id);
        $query  = $this->db->get();
        $data   = array();    
        if ($query->num_rows() > 0)
        {
            foreach (($query->result_array()) as $row) $data[] = $row;
            return $data;
        }
    }

    public function get_all($status)
    {
        $this->db->select("article.*,category.name as namecategory");
        if(count($status) > 0)
        {
            $i = 1;
            foreach ($status as $st) 
            {
                if($i == 1){
                    $this->db->where("article.status", $st);
                    $this->db->where("type","blog");
                }else{
                    $this->db->or_where("article.status", $st);
                    $this->db->where("type","blog");
                }
                $i++;
            }
        }
        $this->db->from("article");
        $this->db->join('category','article.category_id = category.id','left');
        $query  = $this->db->get();
        $data   = $query->result();
        return $data;
    }

    public function get_category()
    {
        $this->db->select("*");
        $this->db->from("category");
        $query  = $this->db->get();
        $data   = $query->result();
        return $data;
    }
    


    public function delete_detail($id)
    {
       $this->db->where('id', $id)
                 ->delete('images');
        return true; 
    }

    public function cek_akses($id_level)
    {   

        $this->db->select('hak_akses');
        $this->db->from('level_access');
        $this->db->where('id_level', $id_level);
        $this->db->limit(1);
        $query = $this->db->get();
        $akses = $query->result();


        return $akses[0]->hak_akses;
    }




}
<?php

class Blogmodel extends CI_Model
{   
    var $table = 'comments';
    var $table2 = 'article';
    public function __construct()
    {
        parent::__construct();
    }

    public function get_detail($slug)
    {

        $this->db->select("*");
        $this->db->select("article.*,category.name as namecategory,images.image");
        $this->db->where("article.status", "active");
        $this->db->where("article.type","blog");
        $this->db->where("article.slug", $slug);
        $this->db->from("article");
        $this->db->join('category','article.category_id = category.id');
        $this->db->join('images','article.id = images.parent_id', 'left');
        $query  = $this->db->get();
        
        $data   = array();    
        if ($query->num_rows() > 0)
        {
            foreach (($query->result_array()) as $row) $data[] = $row;
            return $data;
        }
    }

    public function get_related($slug)
    {
        $category_id            = $this->get_category($slug);

        $this->db->select("article.*,category.name as namecategory,images.image");
        $this->db->where("article.status", "active");
        $this->db->where("article.type","blog");
        $this->db->where("article.category_id",$category_id);
        $this->db->from("article");
        $this->db->join('category','article.category_id = category.id');
        $this->db->join('images','article.id = images.parent_id', 'left');
        $this->db->order_by('article.id','desc');
        $this->db->limit( 4 );
        $query  = $this->db->get();
        $data   = $query->result();
        return $data;
    }


    public function get_category($slug)
    {
        $this->db->select("category_id as cnt");
        $this->db->from("article");
        $this->db->where("slug", $slug);
        $qry = $this->db->get()->result();

        return $qry ? $qry[0]->cnt:0;
    
    }

    public function get_id($name)
    {
        $this->db->select("id as cnt");
        $this->db->from("category");
        $this->db->where("name", $name);
        $qry = $this->db->get()->result();
       
        return $qry ? $qry[0]->cnt:0;
    }

    public function get_blog_popular()
    {
        $world                  = $this->get_id('world');
        $bussiness              = $this->get_id('bussines');
        $sports                 = $this->get_id('sports');
        $technology             = $this->get_id('technology');
        $health                 = $this->get_id('health');
        $lifestyle              = $this->get_id('livestyle');

        $this->db->select("article.*,category.name as namecategory,images.image");
        $this->db->where("article.status", "active");
        $this->db->where("article.type","blog");
        $this->db->group_by('article.category_id');
        $this->db->order_by('article.count_comment','desc');
        $this->db->from("article");
        $this->db->join('category','article.category_id = category.id');
        $this->db->join('images','article.id = images.parent_id', 'left');
        $this->db->where('category_id',$world);
        $this->db->or_where('category_id',$bussiness);
        $this->db->or_where('category_id',$sports);
        $this->db->or_where('category_id',$technology);
        $this->db->or_where('category_id',$health);
        $this->db->or_where('category_id',$lifestyle);
        $this->db->limit(6);
        $query  = $this->db->get();
        $data   = $query->result();
        return $data;
    }

    public function get_datacomments($id)
    {
        $this->db->select("*");
        $this->db->from("comments");
        $this->db->where("article_id",$id);
        $query  = $this->db->get();
        $data   = $query->result();
        return $data;
    }

    public function save($data)
    {
        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }

    public function count_comment($id)
    {
        $this->db->where("id", $id)
                ->set("count_comment", "count_comment + 1", FALSE)
                ->update("article");

        return true;
    }

    public function count_read($slug)
    {
        $this->db->where("slug", $slug)
                ->set("count_read", "count_read + 1", FALSE)
                ->update("article");

        return true;
    }
    public function get_newcomments()
    {
        $this->db->select("*");
        $this->db->order_by("datetime","desc");
        $this->db->from("comments");
        $this->db->limit( 5 );
        $query  = $this->db->get();
        $data   = $query->result();
        return $data;
    }

    public function get_blog_video()
    {
        $this->db->select("article.*,category.name as namecategory");
        $this->db->order_by("datetime","desc");
        $this->db->from("article");
        $this->db->join('category','article.category_id = category.id');
        $this->db->where("video_url !=","");
        $this->db->limit( 4 );
        $query  = $this->db->get();
        $data   = $query->result();
        return $data;
    }
}
<?php

class Homemodel extends CI_Model
{   
    public function __construct()
    {
        parent::__construct();
    }

    public function get_blog_slider()
    {
        $this->db->select("article.*,category.name as namecategory,images.image");
        $this->db->where("article.status", "active");
        $this->db->where("article.type","blog");
        $this->db->group_by('article.category_id');
        $this->db->from("article");
        $this->db->join('category','article.category_id = category.id');
        $this->db->join('images','article.id = images.parent_id', 'left');
        $query  = $this->db->get();
        $data   = $query->result();
        return $data;
    }

    public function count_like($id)
    {
        $this->db->where('id',$id)
                 ->set("count_like","count_like +1", FALSE  )
                 ->update("article");

    }

    public function get_blog_latest()
    {
        $this->db->select("article.*,category.name as namecategory,images.image");
        $this->db->where("article.status", "active");
        $this->db->where("article.type","blog");
        $this->db->order_by('article.id','desc');
        $this->db->from("article");
        $this->db->join('category','article.category_id = category.id');
        $this->db->join('images','article.id = images.parent_id', 'left');
        $this->db->limit( 11 );
        $query  = $this->db->get();
        $data   = $query->result();
        return $data;
    }

     public function get_blog_politics()
    {
        $category_id            = $this->get_category('politics');

        $this->db->select("article.*,category.name as namecategory,images.image");
        $this->db->where("article.status", "active");
        $this->db->where("article.type","blog");
        $this->db->where("article.category_id",$category_id);
        $this->db->from("article");
        $this->db->join('category','article.category_id = category.id');
        $this->db->join('images','article.id = images.parent_id', 'left');
        $this->db->order_by('article.id','desc');
        $this->db->limit( 3 );
        $query  = $this->db->get();
        $data   = $query->result();
        return $data;
    }


    public function get_blog_lifestyle()
    {
        $category_id            = $this->get_category('livestyle');

        $this->db->select("article.*,category.name as namecategory,images.image");
        $this->db->where("article.status", "active");
        $this->db->where("article.type","blog");
        $this->db->where("article.category_id",$category_id);
        $this->db->from("article");
        $this->db->join('category','article.category_id = category.id');
        $this->db->join('images','article.id = images.parent_id', 'left');
        $this->db->order_by('article.id','desc');
        $this->db->limit( 4 );
        $query  = $this->db->get();
        $data   = $query->result();
        return $data;
    }

    public function get_blog_technology()
    {
        $category_id            = $this->get_category('technology');

        $this->db->select("article.*,category.name as namecategory,images.image");
        $this->db->where("article.status", "active");
        $this->db->where("article.type","blog");
        $this->db->where("article.category_id",$category_id);
        $this->db->from("article");
        $this->db->join('category','article.category_id = category.id');
        $this->db->join('images','article.id = images.parent_id', 'left');
        $this->db->order_by('article.id','desc');
        $this->db->limit( 3 );
        $query  = $this->db->get();
        $data   = $query->result();
        return $data;
    }

     public function get_adds()
    {
        $this->db->select("image");
        $this->db->where("ads.status","active");
        $query = $this->db->get("ads");
        $data = $query->result();
        return $data;

    }

    public function get_blog_top()
    {
        $this->db->select("article.*,category.name as namecategory,images.image");
        $this->db->where("article.status", "active");
        $this->db->where("article.type","blog");
        $this->db->order_by('article.count_comment','desc');
        $this->db->from("article");
        $this->db->join('category','article.category_id = category.id');
        $this->db->join('images','article.id = images.parent_id', 'left');
        $this->db->limit( 3 );
        $query  = $this->db->get();
        $data   = $query->result();
        return $data;
    }

    public function get_blog_popular()
    {
        $world                  = $this->get_category('world');
        $bussiness              = $this->get_category('bussines');
        $sports                 = $this->get_category('sports');
        $technology             = $this->get_category('technology');
        $health                 = $this->get_category('health');
        $lifestyle              = $this->get_category('livestyle');

        $this->db->select("article.*,category.name as namecategory,images.image");
        $this->db->where("article.status", "active");
        $this->db->where("article.type","blog");
        $this->db->group_by('article.category_id');
        $this->db->order_by('article.count_comment','desc');
        $this->db->from("article");
        $this->db->join('category','article.category_id = category.id');
        $this->db->join('images','article.id = images.parent_id', 'left');
        $this->db->where('category_id',$world);
        $this->db->or_where('category_id',$bussiness);
        $this->db->or_where('category_id',$sports);
        $this->db->or_where('category_id',$technology);
        $this->db->or_where('category_id',$health);
        $this->db->or_where('category_id',$lifestyle);
        $this->db->limit(6);
        $query  = $this->db->get();
        $data   = $query->result();
        return $data;
    }
    

    public function get_category($name)
    {
        $this->db->select("id as cnt");
        $this->db->from("category");
        $this->db->where("name", $name);
        $qry = $this->db->get()->result();
       
        return $qry ? $qry[0]->cnt:0;
    
    }


    public function get_newcomments()
    {
        $this->db->select("*");
        $this->db->order_by("datetime","desc");
        $this->db->from("comments");
        $this->db->limit( 5 );
        $query  = $this->db->get();
        $data   = $query->result();
        return $data;
    }

    public function get_blog_video()
    {
        $this->db->select("article.*,category.name as namecategory");
        $this->db->order_by("datetime","desc");
        $this->db->from("article");
        $this->db->join('category','article.category_id = category.id');
        $this->db->where("video_url !=","");
        $this->db->limit( 4 );
        $query  = $this->db->get();
        $data   = $query->result();
        return $data;
    }
    public function get_all_gallery($status)
    {
        $this->db->select("*");
        if(count($status) > 0)
        {
            $i = 1;
            foreach ($status as $st) 
            {
                if($i == 1){
                    $this->db->where("status", $st);
                }else{
                    $this->db->or_where("status", $st);
                }
                $i++;
            }
        }
        $this->db->from("gallery");
        $this->db->limit(9);
        $query  = $this->db->get();
        $data   = $query->result();
        return $data;
    }

    public function get_category_all($categoryname)
    {
        $this->db->select("article.*,category.name as namecategory,images.image");
        $this->db->where("article.status", "active");
        $this->db->where("article.type","blog");
        $this->db->where("category.name",$categoryname);
        $this->db->order_by('article.id','desc');
        $this->db->from("article");
        $this->db->join('category','article.category_id = category.id');
        $this->db->join('images','article.id = images.parent_id', 'left');
        $query  = $this->db->get();
        $data   = $query->result();
        return $data;
    }

    public function showCategory($page,$category)
    {
        $offset = 3*$page;
        $limit = 3;

        $sql = "SELECT article.*,category.name as namecategory,images.image FROM article LEFT JOIN category on article.category_id = category.id LEFT JOIN images on article.id = images.parent_id WHERE article.status = 'active' && article.type = 'blog' && category.name = '$category'  ORDER BY article.id DESC limit $offset ,$limit";

        $result = $this->db->query($sql)->result();
        return $result;

    }
}   
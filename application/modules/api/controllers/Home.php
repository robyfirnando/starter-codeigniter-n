<?php

require APPPATH . '/libraries/REST_Controller.php';

class home extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->model('Homemodel', 'model');
    }

    function slider_get() {

        $data = $this->model->get_blog_slider();
         
        if($data)
        {
            $this->response($data, 200);
        }
 
        else
        {
            $this->response(NULL, 404);
        }
    }

    function countlike_put() {

        $result = $this->model->count_like($this->put('id'));
         
        if($result === FALSE)
        {
            $this->response(array('status' => 'failed'));
        }
         
        else
        {
            $this->response(array('status' => 'success'));
        }

    }

    function latest_get() {
        
        $data = $this->model->get_blog_latest();
         
        if($data)
        {
            $this->response($data, 200);
        }
 
        else
        {
            $this->response(NULL, 404);
        }

    }

    function politics_get() {
        
        $data = $this->model->get_blog_politics();
         
        if($data)
        {
            $this->response($data, 200);
        }
 
        else
        {
            $this->response(NULL, 404);
        }
    }

    function lifestyle_get() {
        
        $data = $this->model->get_blog_lifestyle();
         
        if($data)
        {
            $this->response($data, 200);
        }
 
        else
        {
            $this->response(NULL, 404);
        }
    }

    function technology_get() {
        
        $data = $this->model->get_blog_technology();
         
        if($data)
        {
            $this->response($data, 200);
        }
 
        else
        {
            $this->response(NULL, 404);
        }
    }


    function ads_get() {
        
        $data = $this->model->get_adds();
         
        if($data)
        {
            $this->response($data, 200);
        }
 
        else
        {
            $this->response(NULL, 404);
        }
    }

    function top_get() {
        
        $data = $this->model->get_blog_top();
         
        if($data)
        {
            $this->response($data, 200);
        }
 
        else
        {
            $this->response(NULL, 404);
        }
    }

    function popular_get() {
        
        $data = $this->model->get_blog_popular();
         
        if($data)
        {
            $this->response($data, 200);
        }
 
        else
        {
            $this->response(NULL, 404);
        }
    }


    function video_get() {

        $data = $this->model->get_blog_video();
         
        if($data)
        {
            $this->response($data, 200);
        }
 
        else
        {
            $this->response(NULL, 404);
        }
    }


    function comment_get() {
        
        $data = $this->model->get_newcomments();
         
        if($data)
        {
            $this->response($data, 200);
        }
 
        else
        {
            $this->response(NULL, 404);
        }
    }

    function gallery_get() {
        
       $status = array('active');
       $data = $this->model->get_all_gallery($status);
         
        if($data)
        {
            $this->response($data, 200);
        }
 
        else
        {
            $this->response(NULL, 404);
        }
    }

    function category_get() {

        $categoryname = $this->get('categoryname');
        
        $data = $this->model->get_category_all($categoryname);
         
        if($data)
        {
            $this->response($data, 200);
        }
 
        else
        {
            $this->response(NULL, 404);
        }
    }


     function showcategory_get() {

        $category = $this->get('category');
           
        $data = $this->model->showCategory($category);
         
        if($data)
        {
            $this->response($data, 200);
        }
 
        else
        {
            $this->response(NULL, 404);
        }
    }

}


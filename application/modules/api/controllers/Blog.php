<?php

require APPPATH . '/libraries/REST_Controller.php';

class Blog extends REST_Controller {

    var $table = 'comments';
    var $table2 = 'article';

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->model('Blogmodel','model');
    }

    // show data blog
    function index_get() {
        $this->db->select("article.*,category.name as namecategory");
        $this->db->where("article.status", "active");
        $this->db->where("type","blog");
        $this->db->from("article");
        $this->db->join('category','article.category_id = category.id','left');
        $blog = $this->db->get()->result();

        $this->response($blog, 200);
    }

    //show detail blog 
    function detail_get() {
         
        $slug = $this->get('slug');
        $blog = $this->model->get_detail($slug);

        $this->response($blog, 200);
    }

     function category_get()
    {
        $slug = $this->get('slug');
        
        $cat = $this->model->get_category($slug);

        $this->response($cat, 200);
        
    }

    function related_get(){
        $slug = $this->get('slug');

        $data = $this->load->model->get_related($slug);
     
        $this->response($data, 200);
    }
    
    function get_id_get(){

        $name = $this->get('name');

        $id = $this->model->get_id($name);

        $this->response($id, 200);
    }

    function blog_popular_get(){

        $data = $this->model->get_blog_popular();
        
        $this->response($data, 200);
    }

    function datacomments_get()
    {
        $id = $this->get("id");

        $data = $this->model->get_datacomments($id);
        $this->response($data,200);
    }

    function save_post() {
        $data = array(
                    'name'           => $this->post('name'),
                    'email'          => $this->post('email'),
                    'subject'        => $this->post('subject'),
                    'comment'        => $this->post('comment'),
                    'status'         => "active",
                    'datetime'       => date('Y-m-d H:m:s'),
                    'article_id'     => $this->post('article_id'));

        $saving = $this->model->save($data);
        $this->response($saving);

    }

 function count_comment_put() {
        $id = $this->put('id');
    
        $count_comment = $this->model->count_comment($id);
        $this->response($count_comment);
    }

 function count_read_put()
    {
        $slug = $this->put("slug");

        $data = $this->model->count_read($slug);
    
        if($data){
            $this->response($data, 200);
        }
        else{
            $this->response(NULL, 404);
        }
    }

   function newcomments_get()
    {
        $newcomments = $this->model->get_newcomments();
        $this->response($newcomments);
    }

  function blog_video_get()
    {
        $blogvid = $this->model->get_blog_video();
        $this->response($blogvid);
    }


}
<?php

	require_once( APPPATH . 'libraries/facebook-php-sdk-v4-5.0.0/src/Facebook/autoload.php' );
	//session_start();

	function getFacebookLoginUrl()
	{
      $fb = new Facebook\Facebook([
            'app_id' => '542392479283781',
            'app_secret' => '446ca046704f7538254325df23ef7986',
            'default_graph_version' => 'v2.5',
      ]);

      $helper       = $fb->getRedirectLoginHelper();
      $permissions  = ['email', 'public_profile']; // optional
      $callback     = base_url('auth/login_fb').'/';

      return $helper->getLoginUrl($callback, $permissions);
	}

	function getAccessFacebookData()
	{
		  $fb = new Facebook\Facebook([
            'app_id' => '542392479283781',
            'app_secret' => '446ca046704f7538254325df23ef7986',
            'default_graph_version' => 'v2.5',
      ]);

      $helper = $fb->getRedirectLoginHelper();
      try {
        $accessToken  = $helper->getAccessToken();
        $response     = $fb->get('/me?fields=id,email,first_name,last_name,name,gender,picture', $accessToken);
        $user         = $response->getGraphUser();

      } catch(Facebook\Exceptions\FacebookResponseException $e) {
        $accessToken = 'Graph returned an error: ' . $e->getMessage();
        exit;
      } catch(Facebook\Exceptions\FacebookSDKException $e) {
        $accessToken = 'Facebook SDK returned an error: ' . $e->getMessage();
        exit;
      }
      return $user;
	}

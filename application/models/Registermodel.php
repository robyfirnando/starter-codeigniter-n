<?php

class Registermodel extends CI_Model
{   
    public function __construct()
    {
        parent::__construct();
    }

    public function insert()
    {

        $name               = $this->input->post("name");
        $email              = $this->input->post("email");
        $password           = $this->encrypt->encode($this->input->post("password"));
        $levelid            = $this->get_level_id('member');

        $this->db->set("name", $name)
                 ->set("email", $email)
                 ->set("password", $password)
                 ->set("id_level",$levelid)
                 ->set("status", 'non-active')
                 ->set("created_at", date("Y-m-d H:i:s"))
                 ->insert("register");

        $db = $this->db->insert_id();         
        return $db;



    }

    public function get_level_id($name)
    {
        $this->db->select("id as cnt");
        $this->db->from("levels");
        $this->db->where("nama", $name);
        $qry = $this->db->get()->result();
       
        return $qry ? $qry[0]->cnt:0;
    
    }

     public function check_email($email)
    
    {
        $this->db->select("count(0) as cnt");
       
        $this->db->from("register");
       
        $this->db->where("email", $email);
       
        $qry = $this->db->get()->result();
       
        return $qry ? $qry[0]->cnt:0;
    
    }

    function changeActiveState($key)
    {
        $this->load->database();
        $data = array(
           'status' => 'active'
        );

        $this->db->where('md5(id)', $key);
        $this->db->update('register', $data);

        return true;
    }



}
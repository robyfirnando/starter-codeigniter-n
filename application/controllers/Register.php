<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {
	
	public function __construct()
    {
		parent::__construct();
		
		$this->load->library('form_validation');		
		$this->load->model('Registermodel','model');		
		$this->load->library('email');
    }
	
	public function index()
	{
		$this->form_validation->set_rules('name', 'Name', 'trim|required');
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
		$recaptcha = $this->input->post('g-recaptcha-response');
        $response = $this->recaptcha->verifyResponse($recaptcha);

		if ($this->form_validation->run() == FALSE || !isset($response['success']) || $response['success'] <> true) {
            
            $data['scripts']	= '<script src="'. base_url().'assets/backend/js/core/demo/DemoFormComponents.js"></script>
							   		<script src="'. base_url().'assets/backend/js/libs/jquery-validation/dist/jquery.validate.min.js"></script>
							   		<script src="'. base_url().'assets/backend/js/libs/jquery-validation/dist/additional-methods.min.js"></script>';
			$data['message'] 	= (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$data = array(
		           'captcha' => $this->recaptcha->getWidget(), // menampilkan recaptcha
		           'script_captcha' => $this->recaptcha->getScriptTag(), // javascript recaptcha ditaruh di head
		    );


			
			$this->template->load('template', 'register/index', $data);

        } else if ( $this->form_validation->run() == true) {
             
            $id = $this->model->insert();
            
            $encrypted_id = md5($id);

			$config = array();
			$config['charset'] = 'utf-8';
			$config['useragent'] = 'Codeigniter';
			$config['protocol']= "smtp";
			$config['mailtype']= "html";
			$config['smtp_host']= "ssl://smtp.gmail.com";
			$config['smtp_port']= "465";
			$config['smtp_timeout']= "400";
			$config['smtp_user']= "icistest123@gmail.com"; 
			$config['smtp_pass']= "test123123";
			$config['crlf']="\r\n"; 
			$config['newline']="\r\n"; 
			$config['wordwrap'] = TRUE;
			
			$email = $this->input->post("email");

			
			
			$this->email->initialize($config);
			//konfigurasi pengiriman
			$this->email->from($config['smtp_user']);
			
			$this->email->to($email);
			
			$this->email->subject("Verifikasi Akun");
			
			$this->email->message(
				"<table align=center cellpadding=0 cellspacing=0>
					<tbody><tr>			
						<td style=padding-bottom:20px>
							<table cellpadding=0 cellspacing=0>
								<tbody><tr>
									<td style='background:#fff;'>
										<table cellspacing=0 cellpadding=0>
											<tbody><tr>
												<td width=640 align=center>
													<img src=https://t3.ftcdn.net/jpg/00/90/04/66/240_F_90046622_ndUsvkHoQF56Jjs1zwqSCNJIqQ0eV7g8.jpg  width=640 alt= style=display:block;border:0;max-width:100%;min-height:auto class=CToWUd>
												</td>
											</tr>
											<tr>
												<td align=center style='padding:32px 30px 45px'>
													<table cellpadding=0 cellspacing=0>
														<tbody><tr>
															<td width=640 align=center style=background:'#fff'>
																<table cellspacing=0 cellpadding=0>
																	<tbody><tr>
																		<td width=500 align=center style='font-family:Geneva,Tahoma,Verdana,sans-serif;font-size:22px;color:#464646'>
																			Untuk Mengaktifkan Akun anda silahkan Klik Button dibawah ini
																		</td>
																	</tr>
																</tbody></table>
															</td>
														</tr>
														<tr>
										        			<td width=640 align=center style='font-family:Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif;font-size:16px;padding-top:26px'>
										        				<a href=".site_url("register/verification/$encrypted_id")." style='background:#0c99d5;color:#fff;text-decoration:none;border:14px solid #0c99d5;border-left-width:50px;border-right-width:50px;text-transform:uppercase;display:inline-block target=_blank'>
										        					Klik
										        				</a>
															</td>
										        		</tr>
													</tbody></table>
												</td>
											</tr>
										</tbody></table>
									</td>
								</tr>
							</tbody></table>
						</td>
					</tr>
				</tbody></table>"
			);
		
			if($this->email->send())
			{

				$this->session->set_flashdata('success_message', "Berhasil melakukan registrasi, silahkan cek email kamu");
				redirect("home", 'refresh');


			}else
			{
				$this->session->set_flashdata('message', "Berhasil melakukan registrasi, namun gagal mengirim verifikasi email");
				redirect("register", 'refresh');
			}
		
			
        }

		
	}           
	
	public function check_email_user()
	{	
		$email 	= $this->input->post('email');
		$valid 	=  $this->model->check_email($email);
		if($valid > 0){
			$data['status'] 	= FALSE;
			$data['message'] 	= "Email already use, please change the email";
		}else{
			$data['status']		= TRUE;
			$data['message']	= "";
		}
		echo json_encode($data);
	}		
	
	
	public function verification($key)
		
	{
		$this->load->helper('url');

		$this->model->changeActiveState($key);

		$this->session->set_flashdata('success_message', "Selamat kamu telah memverifikasi akun kamu");
		redirect("", 'refresh');


	}




}
	

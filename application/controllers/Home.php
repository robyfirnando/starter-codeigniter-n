<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('Homemodel', 'model');
    }

	public function index()
	{

		$status	= array('active');

		$data['slider']			= $this->model->get_blog_slider();
		$data['latests']		= $this->model->get_blog_latest();
		$data['politics']		= $this->model->get_blog_politics();
		$data['lifestyles']		= $this->model->get_blog_lifestyle();
		$data['technologys']	= $this->model->get_blog_technology();
		$data['tops']			= $this->model->get_blog_top();
		$data['populars']		= $this->model->get_blog_popular();

		$data['gallery']		= $this->model->get_all_gallery($status);
		 
		$data['comments']		= $this->model->get_newcomments();
		$data['videos']		    = $this->model->get_blog_video();
		$data['adds']		    = $this->model->get_adds();



		$this->template->load('template','home/index',$data);

	}


	public function like($id)
	{
		$this->model->count_like($id);
		echo json_encode(TRUE);
	}

	public function category($categoryname)
	 { 
	   //$status = array('active', 'non-active');
	   $data['category'] = $this->model->get_category_all($categoryname);
	   $data['populars']		= $this->model->get_blog_popular();
	   $data['comments']		= $this->model->get_newcomments();
	   $data['videos']		    = $this->model->get_blog_video();

	   $this->template->load('template','category/category',$data);
	 
	 }


	 	public function getCategory(){
        $page = $this->input->get('page');
		$category =  $this->input->get('category');
		$categories = $this->model->showCategory($page,$category);
	        foreach($categories as $kat){
	        	echo '<div class="col-sm-6 col-md-4">
												<div class="post medium-post" id="load">
													<div class="entry-header">
														<div class="entry-thumbnail">
															<a href="'.base_url().'blog/detail/'.$kat->slug. '">'."<img src='".base_url($kat->image)."' class = 'img-responsive'>".'	</div>
													</div>
													<div class="post-content">								
														<div class="entry-meta">
															<ul class="list-inline">
																<li class="publish-date"><a href="#"><i class="fa fa-clock-o"></i> Jan 25, 2016 </a></li>
																<li class="read"><a href="#"><i class="fa fa-eye"></i>'.$kat->count_read.'</a></li>
																<li class="loves" id="like" onclick="like('.$kat->id.')" ><a href=""><i class="fa fa-heart-o"></i>'.$kat->count_like.'</a></li>
															</ul>
														</div>
														<h2 class="entry-title">
															<a href="'.base_url().'blog/detail/'.$kat->slug. '"\>'.$kat->title;
				echo '</a>
														</h2>
													</div>
												</div><!--/post--> 
											</div>';
	        	}
	        exit;
    	}

public function share(){
	$this->load->view("share");
}					
}


?>

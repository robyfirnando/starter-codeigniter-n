<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('Blogmodel','model');
    }

	public function detail($slug)
	{	
		$this->model->count_read($slug);
		
		$blog 				= $this->model->get_detail($slug);
		$data['blog'] 		= $blog[0];
		$data['relateds']	= $this->model->get_related($slug);
		$data['populars']	= $this->model->get_blog_popular();
		$data['comments']	= $this->model->get_newcomments();
		$data['videos']		= $this->model->get_blog_video();
		$data['adds']		= $this->model->get_adds();

		$this->template->load('template','blog/news-details',$data);

	}

	public function like($id)
	{
	 
	 $this->model->count_like($id);
	 echo json_encode(TRUE) ;

	}

	public function ajax_add()
	{	
		$data = array(
				'name' 				=> $this->input->post('name'),
				'email'				=> $this->input->post('email'),
				'subject' 			=> $this->input->post('subject'),
				'comment'			=> $this->input->post('comment'),
				'article_id'		=> $this->input->post('id'),
				'datetime' 		=> date('Y-m-d H:m:s'),

			);
		$insert = $this->model->save($data);
		$this->model->count_comment($this->input->post('id'));
		echo json_encode(array("status" => TRUE));
	}


	public function datacomment($id){
		$data['comments']	= $this->model->get_datacomments($id);
		$this->load->view('blog/comment',$data);	
	}
}

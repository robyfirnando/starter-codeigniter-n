<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->library( array('form_validation','email'));
        $this->load->model('loginmodel','model');			
        
    }

	public function index()
	{
		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
			
        if($this->session->userdata('login') == true){

			redirect('login/google');
		}
		
		if (isset($_GET['code'])) {
			
			$this->googleplus->getAuthenticate();
			
			$this->session->set_userdata('login',true);
			
			$this->session->set_userdata('user_profile',$this->googleplus->getUserInfo());
			
			redirect('login/google');
			
		} 

        $user = $this->model->get_login($this->input->post('email'), $this->input->post('password'));

        if($this->form_validation->run() == true){

			$user = $this->model->get_login($this->input->post('email'), $this->input->post('password'));
			if(is_array($user))
			{
				$this->session->set_userdata("back_email", $user[0]->email);
				$this->session->set_userdata("back_name", $user[0]->name);
				$this->session->set_userdata("back_userid", $user[0]->id);
				$this->session->set_userdata("kk_level", $user[0]->id_level);

				$akses = $this->session->userdata('kk_level');

				if ($akses == 3) {
					redirect("home");
				}

			}else{
				$data['message'] = $user;
				$data['login_url'] = $this->googleplus->loginURL();
				$data['success_message'] = $this->session->flashdata('success_message');
				$this->template->load( 'template', 'login/index', $data);
			}
		}else{
			$data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$data['success_message'] = $this->session->flashdata('success_message');
			$data['login_url'] = $this->googleplus->loginURL();
			$this->template->load( 'template', 'login/index', $data);
		}

	}
	
	public function forgot()
	{

		$this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
		if ( $this->form_validation->run() == true){
			
			$email = $this->input->post('email');
			$id = $this->model->get_by_email($email);
			
		    $encrypted_id = $id;

			$config = array();
			$config['charset'] = 'utf-8';
			$config['useragent'] = 'Codeigniter';
			$config['protocol']= "smtp";
			$config['mailtype']= "html";
			$config['smtp_host']= "ssl://smtp.gmail.com";
			$config['smtp_port']= "465";
			$config['smtp_timeout']= "400";
			$config['smtp_user']= "icistest123@gmail.com"; 
			$config['smtp_pass']= "test123123";
			$config['crlf']="\r\n"; 
			$config['newline']="\r\n"; 
			$config['wordwrap'] = TRUE;
			
			$this->email->initialize($config);
			//konfigurasi pengiriman
			$this->email->from($config['smtp_user']);
			
			$this->email->to($email);
			
			$this->email->subject("change Akun");
			
			$this->email->message(
				"<table align=center cellpadding=0 cellspacing=0>
					<tbody><tr>
						<td style=padding-bottom:20px>
							<table cellpadding=0 cellspacing=0>
								<tbody><tr>
									<td style='background:#fff;'>
										<table cellspacing=0 cellpadding=0>
											<tbody>
											<tr>
												<td align=center style='padding:32px 30px 45px'>
													<table cellpadding=0 cellspacing=0>
														<tbody><tr>
															<td width=640 align=center style=background:'#fff'>
																<table cellspacing=0 cellpadding=0>
																	<tbody><tr>
																		<td width=500 align=center style='font-family:Geneva,Tahoma,Verdana,sans-serif;font-size:22px;color:#464646'>
																			To reset your password, please go to the following page
																		</td>
																	</tr>
																</tbody></table>
															</td>
														</tr>
														<tr>
										        			<td width=640 align=center style='font-family:Trebuchet MS,Lucida Grande,Lucida Sans Unicode,Lucida Sans,Tahoma,sans-serif;font-size:16px;padding-top:26px'>".site_url("login/changepassword/$encrypted_id")." 
															</td>
										        		</tr>
													</tbody></table>
												</td>
											</tr>
										</tbody></table>
									</td>
								</tr>
							</tbody></table>
						</td>
					</tr>
				</tbody></table>"
			);
		
			if($this->email->send())
			{
				$this->session->set_flashdata('success_message', "Untuk reset password silahkan cek email kamu");
				redirect("login", 'refresh');
			}else
			{
				$this->session->set_flashdata('message', "Gagal mengirim email");
				redirect("login", 'refresh');
			}
		
			echo "<br><br><a href='".site_url("login")."'>Kembali ke Menu Login</a>";

		}else{
			$data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$this->template->load( 'template', 'login/forgotpassword', $data);
		}


		
	}

	public function changepassword($id)
	{
		$this->form_validation->set_rules('password', 'Password', 'trim|required');
		
		if ( $this->form_validation->run() == true){

			$this->model->changepassword($id);
            $this->session->set_flashdata('success_message', "changePassword success");
            redirect("login", 'refresh');

		}else{
			
			$single 			= $this->model->get_by_id($id);
			$data['single'] 	= $single[0];

			$this->template->load( 'template', 'login/changepassword',$data);
		}

		
	}


	public function logout()
	{
		$this->session->sess_destroy();
		redirect("login");
	}

	public function login_fb()
	{
		$fb = getAccessFacebookData();
		$data['picture'] 	= $fb['picture'];
		$data['id'] 		= $fb['id'];
		$data['name'] 		= $fb['name'];
		$this->model->insertfb($data);
		$this->template->load( 'template', 'dashboardcostumer', $data);	
	}
 
	public function google()
	{
		$contents = $this->session->userdata('user_profile');
		$data['id']	=$contents['id'];
		$data['picture1']	=$contents['picture'];
		$data['nama1']		=$contents['name'];
		
		$this->model->insertgoogle($data);
		$this->template->load( 'template','home/index',$data);
		
	}
  

}

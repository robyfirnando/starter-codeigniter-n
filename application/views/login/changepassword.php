<section class="section-account">
	<div class="img-backdrop" style="background-image: url('<?php echo base_url();?>assets/backend/img/img16.jpg')"></div>
	<div class="spacer"></div>
	<div class="card contain-sm style-transparent">
		<div class="card-body">
			<div class="row">
				<div class="col-sm-12">
					<br/>
					<span class="text-lg text-bold text-primary">Change Password <?php echo isset($single['name']) ? $single['name'] : ''; ?></span>
					<br/><br/>

					<form class="form floating-label" method="POST" accept-charset="utf-8" method="post" id="form">
						<div class="row">

                        <div class="col-xs-4">      
                            </div>
							<div class="col-xs-4">
								<div class="form-group">
                                <label for="password">Password</label>
                                    <input type="password" class="form-control" id="password" name="password"  data-rule-minlength="6" required>                                    
                                </div>

                                <div class="form-group">
                                <label for="confirm_password">Confirm Password</label>
                                    <input type="password" class="form-control" id="confirm_password" name="confirm_password"  data-rule-minlength="6" required>
                                    
                                    <p class="help-block error_pass" style="color:red; display:none"></p>
                                </div>
                                <button type="reset" class="btn ink-reaction btn-flat btn-primary active reset">RESET</button>
                               <button type="submit" class="btn ink-reaction btn-raised btn-primary submit">SUBMIT</button>
							</div>
							<div class="col-xs-4">

							</div>
						</div>
						<br/>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
<script type="text/javascript">
    $(document).ready(function(){
        
        $(document).on('blur, change', '#confirm_password', function(){
            var pass    = $('#password').val();
            var cpass   = $(this).val();
            if(pass != cpass){
                $('.error_pass').show();
                $('.error_pass').html('Confirm password doesnt same');
                $('#form').find('.submit').attr('disabled', true);
            }else{
                $('.error_pass').hide();
                $('.error_pass').html('');
                $('#form').find('.submit').attr('disabled', false);
            }
        });

        $(document).on('click', '.reset', function(){
            $('.error_dup').hide();
            $('.error_pass').hide();
            $('.error_dup').html('');
            $('.error_pass').html('');
            $('.help-block').html('');
            $('.help-hide').html('');
            $('#form').find('.submit').attr('disabled', false);
        });
        
    })  
</script>
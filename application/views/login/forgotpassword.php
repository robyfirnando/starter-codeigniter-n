<section class="section-account">
	</div>
	<div class="spacer"></div>
	<div class="card contain-sm style-transparent">
		<div class="card-body">
			<div class="row">

			<div class="col-sm-4"></div>

				<div class="col-sm-4">
					<br/>
					<br/><br/>
					<form class="form floating-label" method="POST" accept-charset="utf-8" method="post">
						<div class="form-group">
							<label for="username">Email</label>
							<input type="email" class="form-control" id="email" name="email" required>						</div>
						<br/>
						<div class="row">
							<div class="col-xs-6 text-left">
							</div>
							<div class="col-xs-6 text-right">
								<button class="btn btn-primary btn-raised" type="submit">Submit</button>
							</div>
						</div>
					</form>
				</div>
					<div class="col-sm-4"></div>
			</div>
		</div>
	</div>
</section>

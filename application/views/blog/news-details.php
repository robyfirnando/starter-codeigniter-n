<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v2.7&appId=1035245009925057";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<script async defer src="//assets.pinterest.com/js/pinit.js"></script>
		<div id="newsdetails" class="container-fluid">			
			<div class="section">
				<div class="row">
					<div class="col-sm-9">
						<div id="site-content" class="site-content">
							<div class="row">
								<div class="col-sm-12">
									<div class="left-content">
										<h1 class="section-title title"><?php echo $blog['namecategory']; ?> News</h1>
										<div class="details-news">											
											<div class="post">
												<div class="entry-header">
													<div class="entry-thumbnail">
														<?php if($blog['image']){ ?>
														<img class="img-responsive" src="<?php echo base_url($blog['image']);?>" alt="" />
														<?php }elseif ($blog['video_url']){ ?>
															<div class="entry-thumbnail">
																<iframe src="<?php echo $blog['video_url']; ?>" allowfullscreen></iframe>
															</div>
														<?php } ?>
													</div>
												</div>
												<div class="post-content">								
													<div class="entry-meta">
														<ul class="list-inline">
															<li class="posted-by"><i class="fa fa-user"></i> <a href="#"><?php echo $blog['write_by']; ?></a></li>
															<li class="publish-date"><a href="#"><i class="fa fa-clock-o"></i><?php $date = date_create($blog['datetime']); echo date_format($date,"M d,Y"); ?></a></li>
															<li class="views"><a href="#"><i class="fa fa-eye"></i>
																<?php 
																	$read = $blog['count_read'];
								      								if (strlen($read) >= 4) $read = substr($read, 0, 1) . 'k';
								      								echo $read;
							      								?>
															</a></li>
															<li class="loves" id="like" onclick="like(<?php echo $blog['id'];?>)" ><a href=""><i class="fa fa-heart-o"></i>
																<?php 
																	$like = $blog['count_like'];
								      								if (strlen($like) >= 4) $like = substr($like, 0, 1) . 'k';
								      								echo $like;
							      								?>
															</a></li>
															<li class="comments"><i class="fa fa-comment-o"></i><a href="#">
																<?php 
																	$comment = $blog['count_comment'];
								      								if (strlen($comment) >= 4) $comment = substr($comment, 0, 1) . 'k';
								      								echo $comment;
							      								?>
															</a></li>
														</ul>
													</div>

													<h2 class="entry-title">
														<?php echo $blog['title']; ?>
													</h2>
													<div class="entry-content">
														<?php echo $blog['description']; ?>
														<div class="news-tags">
															<span>Tags:</span>
															<ul class="list-inline">
																<li><a href="#"><?php echo $blog['tags']; ?></a></li>
															</ul>
														</div>
														
														<ul class="list-inline share-link">
														<li><a href="http://www.facebook.com/share.php?u=http://127.0.0.1/starter-codeigniter/blog/detail/<?php echo $blog['slug'];?>&title=<?php echo $blog['title']; ?>" width="71" height="20" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"><img src="<?php echo base_url();?>assets/frontend/images/others/s1.png" alt="" /></a></a></li>
															<li><a href="https://twitter.com/intent/tweet?original_referer=https%3A%2F%2Fdev.twitter.com%2Fweb%2Ftweet-button&ref_src=twsrc%5Etfw&text=Tweet%20Button%20%7C%20Twitter%20Developers&tw_p=tweetbutton&url=http://127.0.0.1/starter-codeigniter/blog/detail/<?php echo $blog['slug'];?>"><img src="<?php echo base_url();?>assets/frontend/images/others/s2.png" alt="" /></a></li>
															<li><a href="https://www.pinterest.com/pinterest/"><img src="<?php echo base_url();?>assets/frontend/images/others/s3.png" alt="" /></a></li>
															<li><a href="https://plus.google.com/share?url=http://127.0.0.1/starter-codeigniter/blog/detail/<?php echo $blog['slug'];?>" onclick="javascript:window.open(this.href,
  															'', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;"><img src="<?php echo base_url();?>assets/frontend/images/others/s4.png" alt="" /></a></li>
														</ul>
													</div>
												</div>
											</div><!--/post--> 
										</div><!--/.section-->
									</div><!--/.left-content-->
								</div>
							</div>
						</div><!--/#site-content-->
						
						<div class="section related-news-section">
							<h1 class="section-title">Related News</h1>
							<div class="related-news">
								<div id="related-news-carousel">
								<?php
									foreach($relateds as $related):
									$date=date_create($related->datetime);
								?>
									<div class="post medium-post">
										<div class="entry-header">
											<div class="entry-thumbnail">
												<a href="<?php echo base_url().'blog/detail/'.$related->slug; ?>">
												<?php if($related->image){ ?>
													<img class="img-responsive" src="<?php echo base_url($related->image);?>" alt="" />
												<?php }else{ ?>
													<img class="img-responsive" src="<?php echo base_url();?>assets/frontend/images/watermark2.png" alt="" />
												<?php } ?>
												</a>
											</div>
										</div>
										<div class="post-content">								
											<div class="entry-meta">
												<ul class="list-inline">
													<li class="publish-date"><a href="#"><i class="fa fa-clock-o"></i> <?php echo date_format($date,"M d,Y"); ?> </a></li>
													<li class="views"><a href="#"><i class="fa fa-eye"></i>
														<?php 
															$read = $related->count_read;
						      								if (strlen($read) >= 4) $read = substr($read, 0, 1) . 'k';
						      								echo $read;
					      								?>
													</a></li>
													<li class="loves" id="like" onclick="like(<?php echo $related->id;?>)" ><a href=""><i class="fa fa-heart-o"></i>
														<?php 
															$like = $related->count_like;
						      								if (strlen($like) >= 4) $like = substr($like, 0, 1) . 'k';
						      								echo $like;
					      								?>
													</a></li>
												</ul>
											</div>
											<h2 class="entry-title">
												<a href="<?php echo base_url().'blog/detail/'.$related->slug; ?>"><?php echo $related->title; ?></a>
											</h2>
										</div>
									</div><!--/post--> 
								<?php endforeach; ?>
								</div>
							</div>
						</div><!--/.section -->	
						
						<div class="row" class="comment">
							<div class="col-sm-12">										
								<div class="comments-wrapper">
									<h1 class="section-title title">Comments</h1>
									<ul class="media-list datacomment">
									</ul>

									<div class="comments-box">
										<h1 class="section-title title">Leave a Comment</h1>
										<form id="comment-form" name="comment-form" method="post" id="form">
											<input type="hidden" name="id" id="id" value="<?php echo $blog['id']; ?>">
											<div class="row">
												<div class="col-sm-4">
													<div class="form-group">
														<label>Name</label>
														<input type="text" name="name" class="form-control" required="required">
													</div>
												</div>
												<div class="col-sm-4">
													<div class="form-group">
														<label>Email</label>
														<input type="email" name="email" class="form-control" required="required">
													</div>
												</div>
												<div class="col-sm-4">
													<div class="form-group">
														<label>Subject</label>
														<input type="text" name="subject" class="form-control">
													</div>
												</div>
												<div class="col-sm-12">
													<div class="form-group">
														<label>Your Text</label>
														<textarea name="comment" id="comment" required="required" class="form-control" rows="5"></textarea>
													</div>
													<div class="text-center">
														<button type="submit" class="btn btn-primary btnsv">Send </button>
													</div>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div><!--/.col-sm-9 -->	
 			
					<div class="col-sm-3">
						<div id="sitebar">							
							<div class="widget">
								<h1 class="section-title title"><a href="listing.html">Most Popular</a></h1>
								<ul class="post-list">
								<?php 	
									foreach($populars as $popular):
								?>
									<li>
										<div class="post small-post">
											<div class="entry-header">
												<div class="entry-thumbnail">
													<a href="<?php echo base_url().'blog/detail/'.$popular->slug; ?>">
													<?php if(!empty($popular->image)): ?>
														<img class="img-responsive" src="<?php echo base_url($popular->image);?>" alt="" /></a>
													<?php endif; ?>
												</div>
											</div>
											<div class="post-content">								
												<div class="video-catagory"><a href="<?php echo base_url().'home/category/'.$popular->namecategory; ?>"><?php echo ucwords(str_replace("-", " ", $popular->namecategory)); ?></a></div>
												<h2 class="entry-title">
													<a href="<?php echo base_url().'blog/detail/'.$popular->slug; ?>"><?php echo $popular->title; ?></a>
												</h2>
											</div>
										</div><!--/post--> 
									</li>
								<?php endforeach; ?>
									
								</ul>
							</div><!--/#widget-->
							<div class="widget">
								<h2 class="section-title">Latest Comments</h2>
								<ul class="comment-list">
									<?php 	
										foreach($comments as $comment):
										$date=date_create($comment->datetime);
									?>
									<li>
										<div class="post small-post">
											<div class="post-content">	
												<div class="entry-meta">
													<ul class="list-inline">
														<li class="post-author"><a href="#"><?php echo $comment->name; ?> ,</a></li>
														<li class="publish-date"><a href="#"><?php echo date_format($date,"M d,Y"); ?> </a></li>
													</ul>
												</div>
												<h2 class="entry-title">
													<a href="news-details.html">
														<?php 
															$comment = $comment->comment;
						      								if (strlen($comment) > 70) $comment = substr($comment, 0, 85) . '...';
						      								echo $comment;
					      								?>
													</a>
												</h2>
											</div>
										</div><!--/post-->
									</li>
									<?php endforeach; ?>
								</ul>
							</div><!-- widget -->
							
							<div class="widget">
								<div class="add">
									<a href="#"><img class="img-responsive" src="<?php echo base_url();?>assets/frontend/images/post/add/add6.jpg" alt="" /></a>
								</div>
							</div>
							<div class="widget">
							<?php 	
								foreach($videos as $video):
							?>
								<div class="post video-post medium-post">
									<div class="entry-header">
										<div class="entry-thumbnail embed-responsive embed-responsive-16by9">
											<iframe class="embed-responsive-item" src="<?php echo $video->video_url; ?>" allowfullscreen></iframe>
										</div>
									</div>
									<div class="post-content">								
										<div class="video-catagory"><a href="#"><?php echo ucwords(str_replace("-", " ", $video->namecategory)); ?></a></div>
										<h2 class="entry-title">
											<a href="news-details.html"><?php echo $video->title; ?></a>
										</h2>
									</div>
								</div><!--/post-->
							<?php endforeach; ?>
							</div>
						</div><!--/#sitebar-->
					</div>
				</div>				
			</div><!--/.section-->
		</div><!--/.container-fluid-->
	
		<div class="footer-top">
			<div class="container-fluid">
				<ul class="list-inline social-icons text-center">
					<li><a href=""><i class="fa fa-facebook"></i>Facebook</a></li>
					<li><a href="#"><i class="fa fa-twitter"></i>Twitter</a></li>
					<li><a href="#"><i class="fa fa-google-plus"></i>Google Plus</a></li>
					<li><a href="#"><i class="fa fa-linkedin"></i>Linkedin</a></li>
					<li><a href="#"><i class="fa fa-pinterest"></i>Pinterest</a></li>
					<li><a href="#"><i class="fa fa-youtube"></i>Youtube</a></li>
				</ul>
			</div>
		</div>
<script src="<?php echo base_url();?>assets/backend/js/libs/jquery/jquery-1.11.2.min.js" ></script>
<script type="text/javascript">
	$(document).ready(function() {

	  	function reload_data(){
	  		var id = $('#id').val();
			var url 		= "<?php echo base_url('blog/datacomment/'); ?>"+id
			$.ajax({
            url: url,
            type: "POST",
            data:  new FormData(this),
            mimeType:"multipart/form-data",
            contentType: false,
            cache: false,
            processData:false,
            dataType: "html",
            beforeSend: function()
            {
            },
            success: function(data)
            {

              $('.datacomment').html(data);
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                  alert('Error adding / update data');
            }         
          });
		}
			
		reload_data();
		
      $("#comment-form").on('submit',(function() {
      
     	var url 		= "<?php echo base_url('blog/ajax_add'); ?>"

          $.ajax({
            url: url,
            type: "POST",
            data:  new FormData(this),
            mimeType:"multipart/form-data",
            contentType: false,
            cache: false,
            processData:false,
            beforeSend: function()
            {
            },
            success: function(data)
            {
              $('#comment-form')[0].reset();
              var newVal = (parseInt($('.comments').text()) +1);
              $('.comments').text(newVal);
              reload_data();
            },
            error: function (jqXHR, textStatus, errorThrown)
            {
                  alert('Error adding / update data');
            }         
          });

      	return false;
 	}));
	})


	function like (id) {
		$.ajax({
			url: "<?php echo site_url('blog/like')?>/"+ id,
			type: "POST",
			dataType: "JSON",
			success: function(data){
				load();
			}				
			
		});
	}

	function load(){
		location.reload();
	}

</script>
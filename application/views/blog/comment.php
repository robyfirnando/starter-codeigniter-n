<?php 	
	foreach($comments as $comment):
	$date = date_create($comment->datetime);
?>
	<li class="media">
		<div class="media-left">
			<a href="#"><img class="media-object" src="<?php echo base_url();?>assets/frontend/images/others/author.png" alt=""></a>
		</div>
		<div class="media-body">
			<h2><a href="#"><?php echo $comment->name; ?></a></h2>
			 <h3 class="date"><a href="#"><?php echo date_format($date,"d F Y"); ?></a></h3>
			<p><?php echo $comment->comment; ?></p>
		</div>
	</li>
<?php endforeach; ?>
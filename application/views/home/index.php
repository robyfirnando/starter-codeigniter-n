	<div class="container-fluid">			
			<div class="section" id="main-slider">
				<?php
					foreach($slider as $blogeone):

				?>
				<div class="post feature-post" style="background-image:url(<?php echo base_url($blogeone->image);?>); background-size:cover;">
					<div class="post-content">
						<div class="catagory"><a href="<?php echo base_url().'home/category/'.$blogeone->namecategory; ?>"><?php echo $blogeone->namecategory; ?></a></div>
						<h2 class="entry-title">
							<a href="<?php echo base_url().'blog/detail/'.$blogeone->slug; ?>"><?php echo $blogeone->title ?></a>
						</h2>
					</div>
				</div><!--/post-->
				<?php endforeach;?>
				
				<?php 
					$image = $blogeone->image;
					if ($image) {
					?>
						</div><!--/post-->
					<?php
					}else{
					?>
						<div class="post feature-post" style="background-image:url(<?php echo base_url();?>assets/frontend/images/watermark2.png); background-size:cover;">
							<div class="post-content">
								<div class="video-icon">
									<a href="<?php echo base_url().'blog/detail/'.$blogeone->slug; ?>"><i class="fa fa-youtube-play"></i></a>
								</div>
								<div class="catagory"><a href=""><?php echo $blogeone->namecategory ?></a></div>
								<h2 class="entry-title">
									<a href="<?php echo base_url().'blog/detail/'.$blogeone->slug; ?>"><?php echo $blogeone->title ?></a>
								</h2>
							</div>
						</div>
					<?php
					} ?>

			</div><!-- #main-slider -->

				<?php 
					$no=1;
					$dataads=  array();

					foreach ($adds as $add) {
					$dataads[$no]['img'] = $add->image;
					$no++;

					}


				 ?>

			<div class="section add inner-add">
				<a href="#"><img class="img-responsive" src="<?php echo base_url($dataads[1]['img']);?>" alt="" /></a>
			</div><!--/.section-->		
			<div class="section">
				<div class="row">
					<div class="col-sm-3">
						<h1 class="section-title title"><a href="listing.html">Latest News</a></h1>	
					
						<div class="left-sidebar">
							<?php
							foreach($latests as $latest):
							$date=date_create($latest->datetime);
							?>
							<div class="post medium-post">
								<div class="entry-header">
									<div class="entry-thumbnail">
										<a href="<?php echo base_url().'blog/detail/'.$latest->slug; ?>">
										<?php if($latest->image){ ?>
											<img class="img-responsive" src="<?php echo base_url($latest->image);?>" alt="" />
										<?php }else{ ?>
											<img class="img-responsive" src="<?php echo base_url();?>assets/frontend/images/watermark2.png" alt="" />
										<?php } ?>
										</a>
									</div>
								</div>
								<div class="post-content">								
									<div class="entry-meta">
										<ul class="list-inline">
											<li class="publish-date"><a href="#"><i class="fa fa-clock-o"></i> <?php echo date_format($date,"M d,Y"); ?></a></li>
											<li class="views"><a href="#"><i class="fa fa-eye"></i>
											<?php 
												$read = $latest->count_read;
			      								if (strlen($read) >= 4) $read = substr($read, 0, 1) . 'k';
			      								echo $read;
		      								?>
											</a></li>
											<li class="loves" id="like" onclick="like(<?php echo $latest->id;?>)" >
											<a href=""><i class="fa fa-heart-o"></i></a>

											<?php 

											$like = $latest->count_like;

												if(strlen($like) >= 4) $like = substr($like, 0,1)
													. 'k';	
												echo $like;	

											 ?>

											</li>
										</ul>
									</div>
									<input type="hidden" name="id" id="id" value="<?php echo $latest->id; ?>">
									<h2 class="entry-title">
										<a href="<?php echo base_url().'blog/detail/'.$latest->slug; ?>"><?php echo $latest->title; ?></a>
									</h2>
								</div>
							</div><!--/post--> 
						<?php endforeach;?>	
						</div><!--/left-sidebar--> 
					
					</div>
					<div class="col-sm-6">
						<div id="site-content" class="site-content">
							<h1 class="section-title title"><a href="listing.html">Top News</a></h1>
							<div class="middle-content">
								<div id="top-news" class="section top-news">
								<?php
									foreach($tops as $top):
									$date=date_create($top->datetime);
								?>
									<div class="post">
										<div class="entry-header">
											<div class="entry-thumbnail">
												<a href="<?php echo base_url().'blog/detail/'.$top->slug; ?>">
												<?php if($top->image){ ?>
													<img class="img-responsive" src="<?php echo base_url($top->image);?>" alt="" />
												<?php }else{ ?>
													<img class="img-responsive" src="<?php echo base_url();?>assets/frontend/images/watermark2.png" alt="" />
												<?php } ?>
												</a>
											</div>
										</div>
										<div class="post-content">								
											<div class="entry-meta">
												<ul class="list-inline">
													<li class="publish-date"><a href="#"><i class="fa fa-clock-o"></i> <?php echo date_format($date,"M d,Y"); ?> </a></li>
													<li class="views"><a href="#"><i class="fa fa-eye"></i>
													<?php 
													$read = $top->count_read;
				      								if (strlen($read) >= 4) $read = substr($read, 0, 1) . 'k';
				      								echo $read;
				      								?>
				      								</a></li>
													<li class="loves" onclick="like(<?php echo $top->id;?>)" >
													<a href=""><i class="fa fa-heart-o"></i>
														<?php 
														$like =$top->count_like;

														if(strlen($like) >=4) $like = substr($like,0,1). 'k';
														echo "$like";
														 ?>
													</a>
													</li>

													 <input type="hidden" name="id" value=" <?php echo  $top->id;  
													 ?> ">

													<li class="comments"><a href="#"><i class="fa fa-comment-o"></i><?php echo $top->count_comment; ?></a></li>
												</ul>
											</div>
											<h2 class="entry-title">
												<a href="<?php echo base_url().'blog/detail/'.$top->slug; ?>"><?php echo $top->title; ?></a>
											</h2>
											<div class="entry-content">
												<p>
													<?php
														$nm = $top->description;
					      								if (strlen($nm) > 150) $nm = substr($nm, 0, 155) . '...';
					      								echo $nm;				
													?>
												</p>
											</div>
										</div>
									</div><!--/post--> 
								<?php endforeach;?>	
									
								</div><!-- top-news -->
								
								<div class="section health-section">
									<h1 class="section-title"><a href="<?php echo base_url().'home/category/politics'; ?>">Politics</a></h1>
									<div class="health-feature">
										<?php
											foreach($politics as $politic):
											$date=date_create($politic->datetime);
										?>
										<div class="post">
											<div class="entry-header">
												<div class="entry-thumbnail">
													<a href="<?php echo base_url().'blog/detail/'.$politic->slug; ?>">
													<?php if($politic->image){ ?>
														<img class="img-responsive" src="<?php echo base_url($politic->image);?>" alt="" />
													<?php }else{ ?>
														<img class="img-responsive" src="<?php echo base_url();?>assets/frontend/images/watermark2.png" alt="" />
													<?php } ?>
													</a>
												</div>
											</div>
											<div class="post-content">								
												<div class="entry-meta">
													<ul class="list-inline">
														<li class="publish-date"><a href="#"><i class="fa fa-clock-o"></i> <?php echo date_format($date,"M d,Y"); ?> </a></li>
														<li class="views"><a href="#"><i class="fa fa-eye"></i>
														<?php 
															$read = $politic->count_read;
						      								if (strlen($read) >= 4) $read = substr($read, 0, 1) . 'k';
						      								echo $read;
					      								?>
														</a></li>
														<li class="loves" onclick="like(<?php echo $politic->id;?>)">
														<a href=""><i class="fa fa-heart-o"></i>

														<?php 

														$like = $politic->count_like;
														if (strlen($like) >=4) $like = substr($like, 0,1) . 'k';
														echo $like;

														 ?>
														</a></li>
													</ul>
												</div>

												<input type="hidden" name="id" value=" <?php echo $politic->id ;  ?> ">

												<h2 class="entry-title">
													<a href="<?php echo base_url().'blog/detail/'.$politic->slug; ?>"><?php echo $politic->title; ?></a>
												</h2>
											</div>
										</div><!--/post--> 
										<?php endforeach; ?>
									</div>
								</div><!--/.health-section -->
								
								<div class="add inner-add">
									<a href="#"><img class="img-responsive" src="<?php echo base_url($dataads[3]['img']);?>" alt="" /></a>

									
								</div><!--/.section-->
								
								<div class="section technology-news">
									<h1 class="section-title"><a href="<?php echo base_url().'home/category/technology'; ?>">Technology</a></h1>
									<div class="row">
										<div class="col-md-8 col-sm-12">
										<?php
					                        $no     = 1;
					                        $data = array();
					                        foreach( $technologys as $technology ){

					                        	$date=date_create($politic->datetime);
					                            $data[$no]['img'] = $technology->image;
					                            $data[$no]['title'] = $technology->title;
					                            $data[$no]['date'] = date_format($date,"M d,Y");
					                            $data[$no]['slug'] = $technology->slug;
					                            $data[$no]['comment'] = $technology->count_comment;
					                            $data[$no]['read'] = $technology->count_read;
					                            $data[$no]['like'] = $technology->count_like;
					                            $nm = $technology->description;
			      								if (strlen($nm) > 100) $nm = substr($nm, 0, 105) . '...';
					                            $data[$no]['description'] = $nm;
					                        $no++;
					                        }

					                    ?>
											<div class="post">
												<div class="entry-header">
													<div class="entry-thumbnail">
														<a href="<?php echo base_url().'blog/detail/'.$data[1]['slug']; ?>"><img class="img-responsive" src="<?php echo base_url($data[1]['img']);?>" alt="" /></a>
													</div>
												</div>
												<div class="post-content">								
													<div class="entry-meta">
														<ul class="list-inline">
															<li class="publish-date"><a href="#"><i class="fa fa-clock-o"></i> <?php echo $data[1]['date']; ?> </a></li>
															<li class="views"><a href="#"><i class="fa fa-eye"></i><?php echo $data[1]['read']; ?></a></li>
															<li class="loves"><a href="#"><i class="fa fa-heart-o"></i><?php echo $data[1]['like']; ?>   </a></li>
															<li class="comments"><a href="#"><i class="fa fa-comment-o"></i><?php echo $data[1]['comment']; ?></a></li>
														</ul>
													</div>
													<h2 class="entry-title">
														<a href="<?php echo base_url().'blog/detail/'.$data[1]['slug']; ?>"><?php echo $data[1]['title']; ?></a>
													</h2>
													<div class="entry-content">
														<p><?php echo $data[1]['description']; ?></p>
													</div>
												</div>
											</div><!--/post--> 
										</div>
										<div class="col-md-4 col-sm-12">
											<div class="post small-post">
												<div class="entry-header">
													<div class="entry-thumbnail">
														<a href="<?php echo base_url().'blog/detail/'.$data[2]['slug']; ?>"><img class="img-responsive" src="<?php echo base_url($data[2]['img']);?>" alt="" /></a>
													</div>
												</div>
												<div class="post-content">								
													<div class="entry-meta">
														<ul class="list-inline">
															<li class="publish-date"><a href="#"><i class="fa fa-clock-o"></i> <?php echo $data[2]['date']; ?> </a></li>
														</ul>
													</div>
													<h2 class="entry-title">
														<a href="<?php echo base_url().'blog/detail/'.$data[2]['slug']; ?>"><?php echo $data[2]['title']; ?></a>
													</h2>
												</div>
											</div><!--/post--> 
											<div class="post small-post">
												<div class="entry-header">
													<div class="entry-thumbnail">
														<a href="<?php echo base_url().'blog/detail/'.$data[3]['slug']; ?>"><img class="img-responsive" src="<?php echo base_url($data[3]['img']);?>" alt="" /></a>
													</div>
												</div>
												<div class="post-content">								
													<div class="entry-meta">
														<ul class="list-inline">
															<li class="publish-date"><a href="#"><i class="fa fa-clock-o"></i> <?php echo $data[3]['date']; ?> </a></li>
														</ul>
													</div>
													<h2 class="entry-title">
														<a href="<?php echo base_url().'blog/detail/'.$data[3]['slug']; ?>"><?php echo $data[3]['title']; ?></a>
													</h2>
												</div>
											</div><!--/post--> 
										</div>
									</div>
								</div><!--/technology-news--> 
								
								<div class="section lifestyle-section">
									<h1 class="section-title"><a href="<?php echo base_url().'home/category/livestyle'; ?>">Life Style</a></h1>
									<div class="row">
										
										<?php 
											foreach($lifestyles as $lifestyle):
											$date=date_create($lifestyle->datetime);
										?>
										<div class="col-md-6">
											<div class="post medium-post">
												<div class="entry-header">
													<div class="entry-thumbnail">
														<a href="<?php echo base_url().'blog/detail/'.$lifestyle->slug; ?>">
														<?php if($lifestyle->image){ ?>
															<img class="img-responsive" src="<?php echo base_url($lifestyle->image);?>" alt="" />
														<?php }else{ ?>
															<img class="img-responsive" src="<?php echo base_url();?>assets/frontend/images/watermark2.png" alt="" />
														<?php } ?>
														</a>
													</div>
												</div>
												<div class="post-content">								
													<div class="entry-meta">
														<ul class="list-inline">
															<li class="publish-date"><a href="#"><i class="fa fa-clock-o"></i> <?php echo date_format($date,"M d,Y"); ?> </a></li>
															<li class="views"><a href="#"><i class="fa fa-eye"></i>
															<?php 
																$read = $lifestyle->count_read;
							      								if (strlen($read) >= 4) $read = substr($read, 0, 1) . 'k';
							      								echo $read;
						      								?>
															</a></li>
															<li class="loves" onclick="like(<?php echo $lifestyle->id;?>)"><a href=""><i class="fa fa-heart-o"></i>
																<?php 
																$like = $lifestyle->count_like;
							      								if (strlen($like) >= 4) $like = substr($like, 0, 1) . 'k';
							      								echo $like;
						      								?>
															</a></li>
														</ul>
													</div>
													<h2 class="entry-title">
														<a href="<?php echo base_url().'blog/detail/'.$lifestyle->slug; ?>"><?php echo $lifestyle->title; ?></a>
													</h2>
												</div>
											</div><!--/post--> 
										</div>
										<?php endforeach; ?>
										

									</div>
								</div><!--/.lifestyle -->
								
								
								<div class="section photo-gallery">
									<h1 class="section-title title"><a href="listing.html">Photo Gallery</a></h1>	
									<div id="photo-gallery" class="carousel slide carousel-fade" data-ride="carousel">	
									<div class="carousel-inner">
									<?php
										$no=0;
										foreach($gallery as $admin):
									?>					
										
											<div class="item <?php if($no == 0) echo 'active'; ?>">
												<a href="#">
													<?php if($admin->image){ ?>
														<img class="img-responsive" src="<?php echo base_url($admin->image);?>" alt="" />
													<?php }else{ ?>
														<img class="img-responsive" src="<?php echo base_url();?>assets/frontend/images/watermark2.png" alt="" />
													<?php } ?>
												</a>
												<h2><a href="#"><?php echo $admin->title; ?></a></h2>
											</div>
										
										<!--/carousel-inner-->

									<?php 
										$no++;
										endforeach;
									?>
									</div>
										<ol class="gallery-indicators carousel-indicators">
											<?php
												$no=0;
												foreach($gallery as $admin):
											?>
											<li data-target="#photo-gallery" data-slide-to="<?php echo $no; ?>" class="<?php if($no == 0) echo 'active'; ?>">
												<?php if($admin->image){ ?>
													<img class="img-responsive" src="<?php echo base_url($admin->image);?>" alt="" />
												<?php }else{ ?>
													<img class="img-responsive" src="<?php echo base_url();?>assets/frontend/images/watermark2.png" alt="" />
												<?php } ?>
											</li>
										<?php 
											$no++;
											endforeach;
										?>
										</ol><!--/gallery-indicators-->
										
										<div class="gallery-turner">
											<a class="left-photo" href="#photo-gallery" role="button" data-slide="prev"><i class="fa fa-angle-left"></i></a>
											<a class="right-photo" href="#photo-gallery" role="button" data-slide="next"><i class="fa fa-angle-right"></i></a>
										</div>
									</div>
								</div><!--/photo-gallery--> 
							</div><!--/.middle-content-->
						</div><!--/#site-content-->
					</div>
					<div class="col-sm-3">
						<div id="sitebar">							
							<div class="widget">
								<h1 class="section-title title"><a href="listing.html">Most Popular</a></h1>
								<ul class="post-list">
								<?php 	
									foreach($populars as $popular):
								?>
									<li>
										<div class="post small-post">
											<div class="entry-header">
												<div class="entry-thumbnail">
													<a href="<?php echo base_url().'blog/detail/'.$popular->slug; ?>">
													<?php if($popular->image){ ?>
														<img class="img-responsive" src="<?php echo base_url($popular->image);?>" alt="" />
													<?php }else{ ?>
														<img class="img-responsive" src="<?php echo base_url();?>assets/frontend/images/watermark2.png" alt="" />
													<?php } ?>
													</a>
												</div>
											</div>
											<div class="post-content">								
												<div class="video-catagory"><a href="<?php echo base_url().'home/category/'.$popular->namecategory; ?>"><?php echo ucwords(str_replace("-", " ", $popular->namecategory)); ?></a></div>
												<h2 class="entry-title">
													<a href="<?php echo base_url().'blog/detail/'.$popular->slug; ?>"><?php echo $popular->title; ?></a>
												</h2>
											</div>
										</div><!--/post--> 
									</li>
								<?php endforeach; ?>
									
								</ul>
							</div><!--/#widget-->
							<div class="widget">
								<h2 class="section-title">Latest Comments</h2>
								<ul class="comment-list">
									<?php 	
										foreach($comments as $comment):
										$date=date_create($comment->datetime);
									?>
									<li>
										<div class="post small-post">
											<div class="post-content">	
												<div class="entry-meta">
													<ul class="list-inline">
														<li class="post-author"><a href="#"><?php echo $comment->name; ?> ,</a></li>
														<li class="publish-date"><a href="#"><?php echo date_format($date,"M d,Y"); ?> </a></li>
													</ul>
												</div>
												<h2 class="entry-title">
													<a href="news-details.html">
														<?php 
															$comment = $comment->comment;
						      								if (strlen($comment) > 70) $comment = substr($comment, 0, 85) . '...';
						      								echo $comment;
					      								?>
													</a>
												</h2>
											</div>
										</div><!--/post-->
									</li>
									<?php endforeach; ?>
								</ul>
							</div><!-- widget -->
							
							<div class="widget">
								<div class="add">
									<a href="#"><img class="img-responsive" src="<?php echo base_url($dataads[4]['img']);?>" alt="" /></a>
								</div>
							</div>
							<div class="widget">
							<?php 	
								foreach($videos as $video):
							?>
								<div class="post video-post medium-post">
									<div class="entry-header">
										<div class="entry-thumbnail embed-responsive embed-responsive-16by9">
											<iframe class="embed-responsive-item" src="<?php echo $video->video_url; ?>" allowfullscreen></iframe>
										</div>
									</div>
									<div class="post-content">								
										<div class="video-catagory"><a href="#"><?php echo ucwords(str_replace("-", " ", $video->namecategory)); ?></a></div>
										<h2 class="entry-title">
											<a href="<?php echo base_url().'blog/detail/'.$video->slug; ?>"><?php echo $video->title; ?></a>
										</h2>
									</div>
								</div><!--/post-->
							<?php endforeach; ?>
							</div>
						</div><!--/#sitebar-->
					</div>
				</div>				
			</div><!--/.section-->

		</div><!--/.container-fluid-->
		
		<div class="footer-top">
			<div class="container-fluid">
				<ul class="list-inline social-icons text-center">
					<li><a href="#"><i class="fa fa-facebook"></i>Facebook</a></li>
					<li><a href="#"><i class="fa fa-twitter"></i>Twitter</a></li>
					<li><a href="#"><i class="fa fa-google-plus"></i>Google Plus</a></li>
					<li><a href="#"><i class="fa fa-linkedin"></i>Linkedin</a></li>
					<li><a href="#"><i class="fa fa-pinterest"></i>Pinterest</a></li>
					<li><a href="#"><i class="fa fa-youtube"></i>Youtube</a></li>
				</ul>
			</div>
		</div><!--/.footer-top-->
	
		
	
</body>
</html>

<script type="text/javascript">

	function like (id) {
		$.ajax({
			url: "<?php echo site_url('home/like')?>/"+ id,
			type: "POST",
			dataType: "JSON",
			success: function(data){
				load();
			}				
			
		});
		
		
	}

	function load(){
		location.reload();
	}

</script>
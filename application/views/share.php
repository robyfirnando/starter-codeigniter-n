<html>
<head>
	<title>Your Website Title</title>
    <!-- You can use Open Graph tags to customize link previews.
    Learn more: https://developers.facebook.com/docs/sharing/webmasters -->
	<meta property="og:url"           content="http://localhost/" />
	<meta property="og:type"          content="website" />
	<meta property="og:title"         content="Judul" />
	<meta property="og:description"   content="Your description" />
	<meta property="og:image"         content="https://www.google.co.id/images/branding/googlelogo/2x/googlelogo_color_272x92dp.png" />
</head>
<body>

	<!-- Load Facebook SDK for JavaScript -->
	<div id="fb-root"></div>
	<script>(function(d, s, id) {
	  var js, fjs = d.getElementsByTagName(s)[0];
	  if (d.getElementById(id)) return;
	  js = d.createElement(s); js.id = id;
	  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1";
	  fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>

	<!-- Your share button code -->
	<?php 
		$actual_link = "$_SERVER[REQUEST_URI]"; 
		$host="http://127.0.0.1";
		// var_dump($host);
		// exit();
	?>
	<div class="fb-share-button" data-href="<?php echo $host.$actual_link; ?>" data-layout="button_count" data-mobile-iframe="true"><a class="fb-xfbml-parse-ignore" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $host.$actual_link; ?>&amp;src=sdkpreparse">Share</a></div>

</body>
</html>


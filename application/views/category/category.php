		<div id="news-listing" class="container-fluid">								
			<div class="section">
				<div class="row">
					<div class="col-sm-9">
						<div id="site-content" class="site-content">
							<h1 class="section-title title">
							<?php
		                        $no     = 1;
		                        $data = array();
		                        foreach( $category as $cat ){
		                            $data[$no]['namecategory'] = $cat->namecategory;
		                        $no++;
		                        }

		                    ?> 
						<a href="listing.html"><?php echo $cat->namecategory; ?> News</a></h1>
						<div class="middle-content" id="middle">								
							<div class="section">
									<div class="row" id="ajax_table">
										<?php
											foreach($category as $admin):
										?>
										     <input type="hidden" name="namecategory" id="namecategory" value="<?php echo $admin->namecategory; ?>">
										<?php 
											endforeach;
										?>
									</div>
								</div><!--/.lifestyle -->
							</div><!--/.middle-content-->
						</div><!--/#site-content-->
					<div class="load-more text-center">
						<button class="btn btn-primary btn-block" id="load_more" data-val="0">Load More News</button>

						<input type="hidden" name="limit" id="limit" value="2"/>
						<input type="hidden" name="offset" id="offset" value="10"/>
					</div>
					</div>
					<div class="col-sm-3">
						<div id="sitebar">							
							<div class="widget">
								<h1 class="section-title title"><a href="listing.html">Most Popular</a></h1>
								<ul class="post-list">
								<?php 	
									foreach($populars as $popular):
								?>
									<li>
										<div class="post small-post">
											<div class="entry-header">
												<div class="entry-thumbnail">
													<a href="<?php echo base_url().'blog/detail/'.$popular->slug; ?>">
													<?php if($popular->image){ ?>
														<img class="img-responsive" src="<?php echo base_url($popular->image);?>" alt="" />
													<?php }else{ ?>
														<img class="img-responsive" src="<?php echo base_url();?>assets/frontend/images/watermark2.png" alt="" />
													<?php } ?>
													</a>
												</div>
											</div>
											<div class="post-content">								
												<div class="video-catagory"><a href="<?php echo base_url().'home/category/'.$popular->namecategory; ?>"><?php echo ucwords(str_replace("-", " ", $popular->namecategory)); ?></a></div>
												<h2 class="entry-title">
													<a href="<?php echo base_url().'blog/detail/'.$popular->slug; ?>"><?php echo $popular->title; ?></a>
												</h2>
											</div>
										</div><!--/post--> 
									</li>
								<?php endforeach; ?>
									
								</ul>
							</div><!--/#widget-->
							<div class="widget">
								<h2 class="section-title">Latest Comments</h2>
								<ul class="comment-list">
									<?php 	
										foreach($comments as $comment):
										$date=date_create($comment->datetime);
									?>
									<li>
										<div class="post small-post">
											<div class="post-content">	
												<div class="entry-meta">
													<ul class="list-inline">
														<li class="post-author"><a href="#"><?php echo $comment->name; ?> ,</a></li>
														<li class="publish-date"><a href="#"><?php echo date_format($date,"M d,Y"); ?> </a></li>
													</ul>
												</div>
												<h2 class="entry-title">
													<a href="news-details.html">
														<?php 
															$comment = $comment->comment;
						      								if (strlen($comment) > 70) $comment = substr($comment, 0, 85) . '...';
						      								echo $comment;
					      								?>
													</a>
												</h2>
											</div>
										</div><!--/post-->
									</li>
									<?php endforeach; ?>
								</ul>
							</div><!-- widget -->
							
							<div class="widget">
								<div class="add">
									<a href="#"><img class="img-responsive" src="<?php echo base_url();?>assets/frontend/images/post/add/add6.jpg" alt="" /></a>
								</div>
							</div>
							<div class="widget">
							<?php 	
								foreach($videos as $video):
							?>
								<div class="post video-post medium-post">
									<div class="entry-header">
										<div class="entry-thumbnail embed-responsive embed-responsive-16by9">
											<iframe class="embed-responsive-item" src="<?php echo $video->video_url; ?>" allowfullscreen></iframe>
										</div>
									</div>
									<div class="post-content">								
										<div class="video-catagory"><a href="#"><?php echo ucwords(str_replace("-", " ", $video->namecategory)); ?></a></div>
										<h2 class="entry-title">
											<a href="<?php echo base_url().'blog/detail/'.$video->slug; ?>"><?php echo $video->title; ?></a>
										</h2>
									</div>
								</div><!--/post-->
							<?php endforeach; ?>
							</div>
						</div><!--/#sitebar-->
					</div>
				</div>	
			</div><!--/.section-->

		</div><!--/.container-fluid-->
		
		<div class="footer-top">
			<div class="container-fluid">
				<ul class="list-inline social-icons text-center">
					<li><a href="#"><i class="fa fa-facebook"></i>Facebook</a></li>
					<li><a href="#"><i class="fa fa-twitter"></i>Twitter</a></li>
					<li><a href="#"><i class="fa fa-google-plus"></i>Google Plus</a></li>
					<li><a href="#"><i class="fa fa-linkedin"></i>Linkedin</a></li>
					<li><a href="#"><i class="fa fa-pinterest"></i>Pinterest</a></li>
					<li><a href="#"><i class="fa fa-youtube"></i>Youtube</a></li>
				</ul>
			</div>
		</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- <script src="<?php echo base_url();?>assets/frontend/vendor/jquery/jquery.min"></script> -->
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/backend/js/libs/jquery/jquery-1.11.2.min.js" ></script>

<script>
    $(document).ready(function(){
    
    getcategory(0);

        $("#load_more").click(function(e){
            e.preventDefault();
            var category = $('#namecategory').val();
            var page = $(this).data('val');
            var data_string = 'page='+ page +'&category='+ category;
            getcategory(page);

        });

    });
	
	
	var page = $(this).data('val');
    var getcategory = function(page){
	var category = $('#namecategory').val();
    var page = $('#load_more').data('val');
    var data_string = 'page='+ page +'&category='+ category;
    var url 		= "<?php echo base_url('home/getCategory/'); ?>"
            
        $.ajax({
            url:url,
            type:'GET',
            data: data_string
        }).done(function(response){
            $("#ajax_table").append(response);
            $('#load_more').data('val', ($('#load_more').data('val')+1));
            scroll();
        });
    };

    var scroll  = function(){
        $('#middle').animate({
            scrollTop: $('#load_more').offset().top
        }, 1000);
    };


    function like (id) {
		$.ajax({
			url: "<?php echo site_url('home/like')?>/"+ id,
			type: "POST",
			dataType: "JSON",
			success: function(data){
				load();
			}				
			
		});
		
		
	}

	function load(){
		location.reload();
	}

</script>
<html>
<head>
    <meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
	<!--title-->
    <title>Daily News | News & Magazine Template</title>
	
	<!--CSS-->
    <link href="<?php echo base_url();?>assets/frontend/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/frontend/css/font-awesome.min.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/frontend/css/magnific-popup.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/frontend/css/owl.carousel.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/frontend/css/main.css" rel="stylesheet">
	<link href="<?php echo base_url();?>assets/frontend/css/responsive.css" rel="stylesheet">	
	<!--Google Fonts-->
	<link href='https://fonts.googleapis.com/css?family=Signika+Negative:400,300,600,700' rel='stylesheet' type='text/css'>
	
    <!--[if lt IE 9]>
	    <script src="js/html5shiv.js"></script>
	    <script src="js/respond.min.js"></script>
    <![endif]-->       
    <link rel="shortcut icon" href="<?php echo base_url();?>assets/frontend/images/ico/favicon.ico">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.png">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.png">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.png">
</head><!--/head-->
<body>
	<div id="main-wrapper">
		<header id="navigation">
			<div class="navbar" role="banner">	
				<div class="container-fluid">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						
						<a class="navbar-brand" href="<?php echo base_url(); ?>home">
							<img class="main-logo img-responsive" src="<?php echo base_url();?>assets/frontend/images/logo.png" alt="">
						</a>
					</div> 
					<nav id="mainmenu" class="navbar-left collapse navbar-collapse"> 
						<a class="secondary-logo" href="<?php echo base_url(); ?>home">
							<img class="img-responsive" src="<?php echo base_url();?>assets/frontend/images/logo.png" alt="">
						</a>
						<ul class="nav navbar-nav">                       
							<li class="dropdown"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Home <span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li><a href="<?php echo base_url();?>home">Home Page Default</a></li>
									<li><a href="index-boxed.html">Home Page <span class="badge">Boxed Layout</span></a></li>
								</ul>
							</li>
							<li class="dropdown"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Pages <span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li><a href="<?php echo base_url();?>blog/detail">News Details Image Post</a></li>
									<li><a href="news-details2.html">News Details Video Post</a></li>
									<li><a href="listing.html">News Listing Page</a></li>
									<li><a href="404.html">404 Error Page</a></li>
									<li role="separator" class="divider"></li>
									<li><a href="news-details-boxed.html">News Details Image <span class="badge">Boxed Layout</span></a></li>
									<li><a href="news-details2-boxed.html">News Details Video <span class="badge">Boxed Layout</span></a></li>
									<li><a href="listing-boxed.html">News Listing Page <span class="badge">Boxed Layout</span></a></li>
									<li><a href="404-boxed.html">404 Error Page <span class="badge">Boxed Layout</span></a></li>
								</ul>
							</li>
							<li class="dropdown mega-dropdown"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">Mega Menu <span class="caret"></span></a>
								<div class="dropdown-menu mega-menu">
									<div class="container-fluid">
										<div class="row">
											<div class="col-sm-3">
												<div class="post medium-post">
													<div class="entry-header">
														<div class="entry-thumbnail">
															<a href="news-details.html"><img class="img-responsive" src="<?php echo base_url();?>assets/frontend/images/post/2.jpg" alt="" /></a>
														</div>
													</div>
													<div class="post-content">								
														<div class="entry-meta">
															<ul class="list-inline">
																<li class="publish-date"><a href="#"><i class="fa fa-clock-o"></i> Jan 5, 2016 </a></li>
																<li class="views"><a href="#"><i class="fa fa-eye"></i>21k</a></li>
																<li class="loves"><a href="#"><i class="fa fa-heart-o"></i>372</a></li>
															</ul>
														</div>
														<h2 class="entry-title">
															<a href="news-details.html">Why is the media so afraid of Facebook?</a>
														</h2>
													</div>
												</div><!--/post--> 
											</div>
											<div class="col-sm-3">
												<div class="post medium-post">
													<div class="entry-header">
														<div class="entry-thumbnail">
															<a href="news-details.html"><img class="img-responsive" src="<?php echo base_url();?>assets/frontend/images/post/3.jpg" alt="" /></a>
														</div>
													</div>
													<div class="post-content">								
														<div class="entry-meta">
															<ul class="list-inline">
																<li class="publish-date"><a href="#"><i class="fa fa-clock-o"></i> Jan 5, 2016 </a></li>
																<li class="views"><a href="#"><i class="fa fa-eye"></i>21k</a></li>
																<li class="loves"><a href="#"><i class="fa fa-heart-o"></i>372</a></li>
															</ul>
														</div>
														<h2 class="entry-title">
															<a href="news-details.html">Why is the media so afraid of Facebook?</a>
														</h2>
													</div>
												</div><!--/post--> 
											</div>
											<div class="col-sm-3">
												<div class="post medium-post">
													<div class="entry-header">
														<div class="entry-thumbnail">
															<a href="news-details.html"><img class="img-responsive" src="<?php echo base_url();?>assets/frontend/images/post/4.jpg" alt="" /></a>
														</div>
													</div>
													<div class="post-content">								
														<div class="entry-meta">
															<ul class="list-inline">
																<li class="publish-date"><a href="#"><i class="fa fa-clock-o"></i> Jan 5, 2016 </a></li>
																<li class="views"><a href="#"><i class="fa fa-eye"></i>21k</a></li>
																<li class="loves"><a href="#"><i class="fa fa-heart-o"></i>372</a></li>
															</ul>
														</div>
														<h2 class="entry-title">
															<a href="news-details.html">Why is the media so afraid of Facebook?</a>
														</h2>
													</div>
												</div><!--/post--> 
											</div>
											<div class="col-sm-3">
												<div class="post medium-post">
													<div class="entry-header">
														<div class="entry-thumbnail">
															<a href="news-details.html"><img class="img-responsive" src="<?php echo base_url();?>assets/frontend/images/post/5.jpg" alt="" /></a>
														</div>
													</div>
													<div class="post-content">								
														<div class="entry-meta">
															<ul class="list-inline">
																<li class="publish-date"><a href="#"><i class="fa fa-clock-o"></i> Jan 5, 2016 </a></li>
																<li class="views"><a href="#"><i class="fa fa-eye"></i>21k</a></li>
																<li class="loves"><a href="#"><i class="fa fa-heart-o"></i>372</a></li>
															</ul>
														</div>
														<h2 class="entry-title">
															<a href="news-details.html">Why is the media so afraid of Facebook?</a>
														</h2>
													</div>
												</div><!--/post--> 
											</div>
										</div>
									</div>
								</div>
							</li>
							<li><a href="<?php echo base_url().'home/category/world'; ?>">World</a></li>
							<li><a href="<?php echo base_url().'home/category/money'; ?>">Money</a></li>
							<li><a href="<?php echo base_url().'home/category/politics'; ?>">Politics</a></li>
							<li><a href="<?php echo base_url().'home/category/bussines'; ?>">Business</a></li>
							<li><a href="<?php echo base_url().'home/category/entertainment'; ?>">Entertainment</a></li>
							<li><a href="<?php echo base_url().'home/category/technology'; ?>">Technology</a></li>
						</ul> 					
					</nav>
					
					<div class="searchNlogin">
						<ul>
							<li class="search-icon"><i class="fa fa-search"></i></li>
							<li class="dropdown user-panel"><a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-user"></i></a>
								<div class="dropdown-menu top-user-section">
									<div class="top-user-form">
										<form id="top-login" role="form">
											<div class="input-group" id="top-login-username">
												<span class="input-group-addon"><img src="<?php echo base_url();?>assets/frontend/images/others/user-icon.png" alt="" /></span>
												<input type="email" class="form-control" placeholder="Email" required="" value=" <?php  echo isset($_POST['email']) ? $_POST['email']: '' ; ?> ">
											</div>
											<div class="input-group" id="top-login-password">
												<span class="input-group-addon"><img src="<?php echo base_url();?>assets/frontend/images/others/password-icon.png" alt="" /></span>
												<input type="password" class="form-control" placeholder="Password" required="">
											</div>
											<div>
												<p class="reset-user">Forgot <a href="#">Password/Username?</a></p>
												<button class="btn btn-danger" type="submit">Login</button>
												
											</div>

											<div>
												
											</div>
										</form>
																		</div>

									<div class="create-account">
										<a href=" <?php  echo getFacebookLoginUrl();?> ">Login With Facebook</a>
									</div>
									<div class="create-account">
										<a href="">Login With Google</a>
								
									</div>
									<div class="create-account">
										<a href=" <?php echo base_url().'register';?>">Create a New Account</a>
									</div>
								</div>
							</li>
						</ul>
						<div class="search">
							<form role="form">
								<input type="text" class="search-form" autocomplete="off" placeholder="Type & Press Enter">
							</form>
						</div> <!--/.search--> 
					</div><!--.searchNlogin -->
				</div>	
			</div>
		</header><!--/#navigation--> 

		<?php echo $contents; ?>


		<!--/.footer-top-->
	
		<footer id="footer">
			<div class="bottom-widgets">
				<div class="container-fluid">
					<div class="row">
						<div class="col-sm-4">
							<div class="widget">
								<h2>Categories</h2> 
								<ul>
									<li><a href="#">Business</a></li>
									<li><a href="#">Politics</a></li>
									<li><a href="#">Sports</a></li>
									<li><a href="#">World</a></li>
									<li><a href="#">Technology</a></li>
								</ul>
								<ul>
									<li><a href="#">Environment</a></li>
									<li><a href="#">Health</a></li>
									<li><a href="#">Entertainment</a></li>
									<li><a href="#">Lifestyle</a></li>
								</ul>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="widget">
								<h2>Editions</h2> 
								<ul>
									<li><a href="#">United States</a></li>
									<li><a href="#">China</a></li>
									<li><a href="#">India</a></li>
									<li><a href="#">Maxico</a></li>
									<li><a href="#">Middle East</a></li>
								</ul>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="widget">
								<h2>Popular Tags</h2> 
								<ul>
									<li><a href="#">Gallery</a></li>
									<li><a href="#">Sports</a></li>
									<li><a href="#">Featured</a></li>
									<li><a href="#">World</a></li>
									<li><a href="#">Fashion</a></li>
								</ul>
								<ul>
									<li><a href="#">Environment</a></li>
									<li><a href="#">Health</a></li>
									<li><a href="#">Entertainment</a></li>
									<li><a href="#">Lifestyle</a></li>
									<li><a href="#">Business</a></li>
								</ul>
								<ul>
									<li><a href="#">Tech</a></li>
									<li><a href="#">Movie</a></li>
									<li><a href="#">Music</a></li>
								</ul>
							</div>
						</div>
						<div class="col-sm-2">
							<div class="widget">
								<h2>Products</h2> 
								<ul>
									<li><a href="#">Ebook</a></li>
									<li><a href="#">Baby Product</a></li>
									<li><a href="#">Magazine</a></li>
									<li><a href="#">Sports Elements</a></li>							
									<li><a href="#">Technology</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="footer-bottom">
				<div class="container-fluid text-center">
					<p>Copyright &copy; 2016,  DailyNews. Developed by <a href="http://themeregion.com/">ThemeRegion</a></p>
				</div>
			</div>		
		</footer>	
	</div><!--/#main-wrapper--> 
	
		
	<!--/#scripts--> 
    <script type="text/javascript" src="<?php echo base_url();?>assets/frontend/js/jquery.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/frontend/js/bootstrap.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/frontend/js/jquery.magnific-popup.min.js"></script>
	<script type="text/javascript" src="<?php echo base_url();?>assets/frontend/js/owl.carousel.min.js"></script> 
    <script type="text/javascript" src="<?php echo base_url();?>assets/frontend/js/main.js"></script>
    <script type="text/javascript" src="<?php echo base_url();?>assets/backend/js/libs/jquery-1.11.2.min.js"></script>

	<script>
	  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	  ga('create', 'UA-73239902-1', 'auto');
	  ga('send', 'pageview');

	</script>




</body>


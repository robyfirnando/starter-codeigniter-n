<section class="section-account">
	<div class="img-backdrop" style="background-image: url('<?php echo base_url();?>assets/backend/img/img16.jpg')"></div>
	<div class="spacer"></div>
	<div class="card contain-sm style-transparent">
		<div class="card-body">
			<div class="row">
				<div class="col-sm-12">
					<br/>
					<span class="text-lg text-bold text-primary">FORGOT PASSWORD</span>
					<br/><br/>
					<form class="form floating-label" method="POST" accept-charset="utf-8" method="post">
						<div class="form-group">
							<input type="email" class="form-control" id="email" name="email" required>
							<label for="username">Email</label>
						</div>
						<br/>
						<div class="row">
							<div class="col-xs-6 text-left">
							</div>
							<div class="col-xs-6 text-right">
								<button class="btn btn-primary btn-raised" type="submit">Submit</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>

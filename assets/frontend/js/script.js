// Agency Theme JavaScript

(function ($) {
    "use strict"; // Start of use strict

    // jQuery for page scrolling feature - requires jQuery Easing plugin
    $('a.page-scroll').bind('click', function (event) {
        var $anchor = $(this);
        $('html, body').stop().animate({
            scrollTop: ($($anchor.attr('href')).offset().top - 50)
        }, 1250, 'easeInOutExpo');
        event.preventDefault();
    });

    // Highlight the top nav as scrolling occurs
    $('body').scrollspy({
        target: '.navbar-fixed-top',
        offset: 51
    });

    // Closes the Responsive Menu on Menu Item Click
    $('.navbar-collapse ul li a:not(.dropdown-toggle)').click(function () {
        $('.navbar-toggle:visible').click();
    });

    // Offset for Main Navigation
    $('#mainNav').affix({
        offset: {
            top: 100
        }
    })

    $('#joinmember').bootstrapValidator({
        //  live: 'disabled',
        message: 'This value is not valid',
        fields: {
            fname: {
                validators: {
                    notEmpty: {
                        message: 'Required field'
                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: 'Required field'
                    },
                    emailAddress: {
                        message: 'Required field'
                    }
                }
            },
            age: {
                validators: {
                    notEmpty: {
                        message: 'Required field'
                    }
                }
            },
            phone: {
                validators: {
                    notEmpty: {
                        message: 'Required field'
                    }
                }
            },
            city: {
                validators: {
                    notEmpty: {
                        message: 'Required field'
                    }
                }
            },
            status: {
                validators: {
                    notEmpty: {
                        message: 'Required field'
                    }
                }
            }
        },
        onSuccess: function (e, data) {
            var form = $('#joinmember');
            $.ajax({
                url: form.attr('action'),
                type: 'POST',
                data: form.serialize(),
                success: function (result) {
                    $('#notif').modal('show');
                    $('#joinmember').trigger('reset');
                    $('#joinmember').bootstrapValidator('resetForm', true);
                }
            });
        }
    });

    $('#joinmember').submit(function (e) {
        e.preventDefault();
    });

    $('#joinvolunteer').bootstrapValidator({
        //  live: 'disabled',
        message: 'This value is not valid',
        fields: {
            fname: {
                validators: {
                    notEmpty: {
                        message: 'Required field'
                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: 'Required field'
                    },
                    emailAddress: {
                        message: 'Required field'
                    }
                }
            },
            age: {
                validators: {
                    notEmpty: {
                        message: 'Required field'
                    }
                }
            },
            phone: {
                validators: {
                    notEmpty: {
                        message: 'Required field'
                    }
                }
            },
            city: {
                validators: {
                    notEmpty: {
                        message: 'Required field'
                    }
                }
            },
        },
        onSuccess: function (e, data) {
            var form = $('#joinvolunteer');
            $.ajax({
                url: form.attr('action'),
                type: 'POST',
                data: form.serialize(),
                success: function (result) {
                    $('#notif').modal('show');
                    $('#joinvolunteer').trigger('reset');
                    $('#joinvolunteer').bootstrapValidator('resetForm', true);
                }
            });
        }
    });

    $('#joinvolunteer').submit(function (e) {
        e.preventDefault();
    });


    $('#pinktalkform').bootstrapValidator({
        //  live: 'disabled',
        message: 'This value is not valid',
        fields: {
            fname: {
                validators: {
                    notEmpty: {
                        message: 'Required field'
                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: 'Required field'
                    },
                    emailAddress: {
                        message: 'Invalid email add'
                    }
                }
            },
            jabatan: {
                validators: {
                    notEmpty: {
                        message: 'Required field'
                    }
                }
            },
            phone: {
                validators: {
                    notEmpty: {
                        message: 'Required field'
                    }
                }
            },
            jumlah: {
                validators: {
                    notEmpty: {
                        message: 'Required field'
                    }
                }
            },
            add: {
                validators: {
                    notEmpty: {
                        message: 'Required field'
                    }
                }
            }
        },
        onSuccess: function (e, data) {
            var form = $('#pinktalkform');
            $.ajax({
                url: form.attr('action'),
                type: 'POST',
                data: form.serialize(),
                success: function (result) {
                    $('#notif').modal('show');
                    $('#pinktalkform').trigger('reset');
                    $('#pinktalkform').bootstrapValidator('resetForm', true);
                }
            });
        }
    });

    $('#pinktalkform').submit(function (e) {
        e.preventDefault();
    });

    $('#contactf').bootstrapValidator({
        //  live: 'disabled',
        message: 'This value is not valid',
        fields: {
            fname: {
                validators: {
                    notEmpty: {
                        message: 'Required field'
                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: 'Required field'
                    },
                    emailAddress: {
                        message: 'Required field'
                    }
                }
            },
            subject: {
                validators: {
                    notEmpty: {
                        message: 'Required field'
                    }
                }
            },
            msg: {
                validators: {
                    notEmpty: {
                        message: 'Required field'
                    }
                }
            },
        },
        onSuccess: function (e, data) {
            var form = $('#contactf');
            $.ajax({
                url: form.attr('action'),
                type: 'POST',
                data: form.serialize(),
                success: function (result) {
                    $('#notif').modal('show');
                    $('#contactf').trigger('reset');
                    $('#contactf').bootstrapValidator('resetForm', true);
                }
            });
        }
    });

    $('#contactf').submit(function (e) {
        e.preventDefault();
    });

})(jQuery); // End of use strict